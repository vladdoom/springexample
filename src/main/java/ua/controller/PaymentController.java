package ua.controller;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ua.dao.Firm;
import ua.dao.Payment;
import ua.service.FirmEditor;
import ua.service.FirmService;
import ua.service.PaymentService;
import ua.utility.Document;
import ua.validator.PaymentValidator;
@Controller
public class PaymentController {
	@Autowired
	PaymentService paymentService;
	@Autowired
	FirmService firmService;
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		 binder.setValidator(new PaymentValidator(paymentService));
		 binder.registerCustomEditor(Firm.class, new FirmEditor(firmService));
	}
	@RequestMapping(value = "/payment", method = RequestMethod.POST)
	public String createPayment(Model  model, Principal principal, @Valid @ModelAttribute("payment") Payment payment,  BindingResult result) {
		payment.setUserid(Integer.parseInt(principal.getName()));
		if (result.hasErrors()) {
			model.addAttribute("handelist", paymentService.findAllByUserid(principal));
			model.addAttribute("payment",payment);
			model.addAttribute("listfirm", firmService.findAllByUserid(principal));
	        return "/payment";
	    }
		paymentService.save(payment);
		return "redirect:/payment";
	}

	
	@RequestMapping(value = "/payment", method = RequestMethod.GET)
	public String handlePayment(Model model, Principal principal) {
		if (principal==null){System.out.println("User no autitification___");}else{
		model.addAttribute("handelist", paymentService.findAllByUserid(principal));
		Payment payment = new Payment();
		payment.setDate(getDate());
		model.addAttribute("payment",payment);
		model.addAttribute("listfirm", firmService.findAllByUserid(principal));
		model.addAttribute("document", Document.PAYMENT);}
		return "payment";
	}
	@RequestMapping(value = "/editpayment", method = RequestMethod.GET)
	public String EditPayment(Model model, Principal principal,@RequestParam("id") int id) {
		model.addAttribute("handelist", paymentService.findAllByUserid(principal));
		Payment payment = paymentService.findById(id);
		payment.setSumatransient(String.valueOf(payment.getSumapay()));
		model.addAttribute("payment",payment);
		model.addAttribute("listfirm", firmService.findAllByUserid(principal));
		model.addAttribute("document", Document.PAYMENT);
		return "payment";
	}
	@RequestMapping(value = "/editpayment", method = RequestMethod.POST)
	public String updatePayment(Model model, Principal principal, @Valid @ModelAttribute("payment") Payment payment,  BindingResult result) {
		payment.setUserid(Integer.parseInt(principal.getName()));
		if (result.hasErrors()) {
			model.addAttribute("handelist", paymentService.findAllByUserid(principal));
			model.addAttribute("payment",payment);
			model.addAttribute("listfirm", firmService.findAllByUserid(principal));
	        return "/payment";
	    }
		paymentService.update(payment);
		return "redirect:/payment";
	}
	public String getDate(){
		 Date date = new Date();
		  SimpleDateFormat formater = new SimpleDateFormat("dd.MM.yyyy");
		 return formater.format(date);
	}
}
