package ua.controller;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ua.dao.BasisIncome;
import ua.dao.Employee;
import ua.dao.IncomeOrder;
import ua.service.BasisIncomeService;
import ua.service.EmployeeService;
import ua.service.IncomeOrderService;
import ua.service.editor.BasisIncomeEditor;
import ua.service.editor.EmployeeEditor;
import ua.utility.Document;
import ua.utility.FormatDate;
import ua.validator.IncomeOrderValidator;

@Controller
public class IncomeOrderController {
	@Autowired
	IncomeOrderService incomeOrderService;
	@Autowired
	BasisIncomeService basisIncomeService;
	@Autowired
	EmployeeService employeeService;
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		 binder.setValidator(new IncomeOrderValidator(incomeOrderService));
		binder.registerCustomEditor(BasisIncome.class, new BasisIncomeEditor(basisIncomeService));
		binder.registerCustomEditor(Employee.class, new EmployeeEditor(employeeService));
	}

	@RequestMapping(value = "/incomeorder", method = RequestMethod.POST)
	public String createIncomeOrder(Model model, Principal principal,@Valid @ModelAttribute(value = "incomeorder") IncomeOrder incomeorder, BindingResult result) {
		incomeorder.setUserid(Integer.parseInt(principal.getName()));
		if (result.hasErrors()) {
			model.addAttribute("handelist", incomeOrderService.findAllByUserid(principal));
			model.addAttribute("incomeorder", incomeorder);
			model.addAttribute("basisIncome", basisIncomeService.findAllByUserid(principal));
			model.addAttribute("employees", employeeService.findAllByUserid(principal));
	        return "/incomeorder";
	    }
		
		incomeOrderService.save(incomeorder);
		return "redirect:/incomeorder";
	}

	@RequestMapping(value = "/incomeorder", method = RequestMethod.GET)
	public String handleIncomeorder(Model model, Principal principal ) {
		if (principal==null){System.out.println("User no autitification___");}else{
		model.addAttribute("handelist", incomeOrderService.findAllByUserid(principal));
		IncomeOrder incomeOrder = new IncomeOrder();
		incomeOrder.setDates(getDate());
		model.addAttribute("incomeorder", incomeOrder);
		model.addAttribute("basisIncome", basisIncomeService.findAllByUserid(principal));
		model.addAttribute("employees", employeeService.findAllByUserid(principal));
		model.addAttribute("document", Document.INCOMEORDER);}
		return "incomeorder";
	}

	@RequestMapping(value = "/editincomeorder", method = RequestMethod.POST)
	public String updateIncomeOrder(Model model, Principal principal,@Valid @ModelAttribute(value = "incomeorder") IncomeOrder incomeorder, BindingResult result) {
		incomeorder.setUserid(Integer.parseInt(principal.getName()));
		if (result.hasErrors()) {
			model.addAttribute("handelist", incomeOrderService.findAllByUserid(principal));
			model.addAttribute("incomeorder", incomeorder);
			model.addAttribute("basisIncome", basisIncomeService.findAllByUserid(principal));
			model.addAttribute("employees", employeeService.findAllByUserid(principal));
	        return "/incomeorder";
	    }
		
		incomeOrderService.update(incomeorder);
		return "redirect:/incomeorder";
	}

	@RequestMapping(value = "/editincomeorder", method = RequestMethod.GET)
	public String viewIncomeorder(Model model, Principal principal,@RequestParam("id") int id) {
		model.addAttribute("handelist", incomeOrderService.findAllByUserid(principal));
		IncomeOrder incomeOrder = incomeOrderService.findById(id);
		incomeOrder.setSumatransient(String.valueOf(incomeOrder.getSuma()));
		model.addAttribute("incomeorder", incomeOrder);
		model.addAttribute("basisIncome", basisIncomeService.findAllByUserid(principal));
		model.addAttribute("employees", employeeService.findAllByUserid(principal));
		model.addAttribute("document", Document.INCOMEORDER);
		return "incomeorder";
	}
	public String getDate(){
		 Date date = new Date();
		  SimpleDateFormat formater = new SimpleDateFormat("dd.MM.yyyy");
		 return formater.format(date);
	}
}
