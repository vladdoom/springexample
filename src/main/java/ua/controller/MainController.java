package ua.controller;

import java.io.UnsupportedEncodingException;
import java.security.Principal;
//import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ua.dao.BasisIncome;
import ua.dao.BasisSpend;
import ua.dao.Employee;
import ua.dao.Firm;
import ua.service.BankService;
import ua.service.BasisIncomeService;
import ua.service.BasisSpendService;
import ua.service.CommodityService;
import ua.service.EmployeeService;
import ua.service.FirmService;
import ua.service.IncomeService;
import ua.service.ProviderService;
import ua.service.TillService;
import ua.service.UsersService;
import ua.service.editor.BasisIncomeEditor;
import ua.service.editor.BasisSpendEditor;
import ua.service.editor.EmployeeEditor;
import ua.utility.Document;

@Controller
public class MainController {
	@Autowired
	BankService bankService;
	@Autowired
	BasisIncomeService basisIncomeService;
	@Autowired
	BasisSpendService basisSpendService;
	@Autowired
	EmployeeService employeeService;
	@Autowired
	FirmService firmService;
	@Autowired
	TillService tillService;
	@Autowired
	UsersService usersService;
	@Autowired
	ProviderService providerService;
	@Autowired
	CommodityService commodityService;
	@Autowired
	IncomeService incomeService;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		// binder.registerCustomEditor(Firm.class, new FirmEditor(firmService));
		binder.registerCustomEditor(BasisIncome.class, new BasisIncomeEditor(basisIncomeService));
		binder.registerCustomEditor(BasisSpend.class, new BasisSpendEditor(basisSpendService));
		binder.registerCustomEditor(Employee.class, new EmployeeEditor(employeeService));
		// binder.setValidator(new PaymentValidator(paymentService));

	}

	@RequestMapping(value = "/reglist", method = RequestMethod.GET)
	public String handleUsers(Model model, Principal principal) {
		model.addAttribute("handelist", usersService.findAllList());
		model.addAttribute("document", Document.USERS);
		return "reglist";
	}

	@RequestMapping(value = "/till", method = RequestMethod.GET)
	public String handleTill(Model model, Principal principal) {
		if (principal==null){System.out.println("User no autitification___");}else{
		model.addAttribute("handelist", tillService.findAllByUserid(principal));}
		return "till";
			/*model.addAttribute("handelist", tillService.findNew2(principal));}
		return "till2";*/
	}

	@RequestMapping(value = "/provider", method = RequestMethod.GET)
	public String handleProvider(Model model, Principal principal) {
		if (principal==null){System.out.println("User no autitification___");}else{
		model.addAttribute("handelist", providerService.findAllByUserid(principal));}
		return "provider";
	}

	@RequestMapping(value = "/commodity", method = RequestMethod.GET)
	public String readeCommodity(Model model, Principal principal) {
		if (principal==null){System.out.println("User no autitification___");}else{
		model.addAttribute("handelist", commodityService.findAllByUserid(principal));}
		return "commodity";
	}

	@RequestMapping(value = "/income", method = RequestMethod.GET)
	public String readeIncome(Model model, Principal principal) {
		if (principal==null){System.out.println("User no autitification___");}else{
		model.addAttribute("handelist", incomeService.findAllByUserid(principal));}
		return "income";
	}

	@RequestMapping(value = "/bank", method = RequestMethod.GET)
	public String readBank(Model model, Principal principal) {
		if (principal==null){System.out.println("User no autitification___");}else{
		model.addAttribute("handelist", bankService.findAllByUserid(principal));}
		return "bank";
	}

	@RequestMapping(value = "/basisIncome", method = RequestMethod.POST)
	public String createBasisIncome(Principal principal, @ModelAttribute("basisIncome") BasisIncome basisIncome) throws UnsupportedEncodingException {
		basisIncome.setUserid(Integer.parseInt(principal.getName()));
		System.out.println(basisIncome.getBasis()+basisIncome.getId());
		basisIncomeService.save(basisIncome);
		return "redirect:/basisIncome";
	}

	@RequestMapping(value = "/editbasisSpend", method = RequestMethod.POST)
	public String editBasisSpend(Principal principal, @ModelAttribute("basisSpend") BasisSpend basisSpend, @RequestParam Integer id) {
		basisSpend.setUserid(Integer.parseInt(principal.getName()));
		System.out.println(basisSpend.getBasis()+basisSpend.getId()+"_________"+id);
		basisSpendService.save(basisSpend);
		return "redirect:/basisSpend";
	}
	@RequestMapping(value = "/editbasisSpend", method = RequestMethod.GET)
	public String upBasisSpend(Model model, Principal principal, @RequestParam Integer id) {
	model.addAttribute("handelist", basisSpendService.findAllByUserid(principal));
	model.addAttribute("basisSpend", basisSpendService.findById(id));
	return "/basisSpend";}
	
	@RequestMapping(value = "/basisIncome", method = RequestMethod.GET)
	public String readBasisIncome(Model model, Principal principal) {
		if (principal==null){System.out.println("User no autitification___");}else{
		model.addAttribute("handelist", basisIncomeService.findAllByUserid(principal));
		model.addAttribute("basisIncome", new BasisIncome());
		model.addAttribute("document", Document.BASISINCOME);}
		return "basisIncome";
	}

	@RequestMapping(value = "/basisSpend", method = RequestMethod.POST)
	public String createBasisSpend(Principal principal, @ModelAttribute("basisSpend") BasisSpend basisSpend) {
		basisSpend.setUserid(Integer.parseInt(principal.getName()));
		basisSpendService.save(basisSpend);
		return "redirect:/basisSpend";
	}

	@RequestMapping(value = "/basisSpend", method = RequestMethod.GET)
	public String readeBasisSpend(Model model, Principal principal) {
		if (principal==null){System.out.println("User no autitification___");}else{
		model.addAttribute("handelist", basisSpendService.findAllByUserid(principal));
		model.addAttribute("basisSpend", new BasisSpend());}

		return "basisSpend";
	}

	@RequestMapping(value = "/editEmployee", method = RequestMethod.POST)
	public String editEmployee(Principal principal, @ModelAttribute("employee") Employee employee, @RequestParam Integer id) {
		employee.setUserid(Integer.parseInt(principal.getName()));
		employeeService.save(employee);
		return "redirect:/employee";
	}
	@RequestMapping(value = "/editEmployee", method = RequestMethod.GET)
	public String upEmployee(Model model, Principal principal, @RequestParam("id") Integer id) {
	model.addAttribute("handelist", employeeService.findAllByUserid(principal));
	model.addAttribute("employee", employeeService.findById(id));
	return "/employee";}
	
	@RequestMapping(value = "/employee", method = RequestMethod.POST)
	public String createEmployee(Principal principal, @ModelAttribute("employee") Employee employee) {
		employee.setUserid(Integer.parseInt(principal.getName()));
		employeeService.save(employee);
		return "redirect:/employee";
	}

	@RequestMapping(value = "/employee", method = RequestMethod.GET)
	public String readeEmployee(Model model, Principal principal) {
		if (principal==null){System.out.println("User no autitification___");}else{
		model.addAttribute("handelist", employeeService.findAllByUserid(principal));
		model.addAttribute("employee", new Employee());}
		return "employee";
	}

	@RequestMapping(value = "/editFirm", method = RequestMethod.POST)
	public String editFirm(Principal principal, @ModelAttribute("firm") Firm firm, @RequestParam Integer id) {
		firm.setUserid(Integer.parseInt(principal.getName()));
		firmService.save(firm);
		return "redirect:/firm";
	}
	@RequestMapping(value = "/editFirm", method = RequestMethod.GET)
	public String upFirm(Model model, Principal principal, @RequestParam Integer id) {
	model.addAttribute("handelist", firmService.findAllByUserid(principal));
	model.addAttribute("firm", firmService.findById(id));
	return "/firm";}
	
	@RequestMapping(value = "/firm", method = RequestMethod.POST)
	public String createFirm(Principal principal, @ModelAttribute("firm") Firm firm) {
		firm.setUserid(Integer.parseInt(principal.getName()));
		firmService.save(firm);
		return "redirect:/firm";
	}

	@RequestMapping(value = "/firm", method = RequestMethod.GET)
	public String handleFirm(Model model, Principal principal) {
		if (principal==null){System.out.println("User no autitification___");}else{
		model.addAttribute("handelist", firmService.findAllByUserid(principal));
		model.addAttribute("firm", new Firm());}

		return "firm";
	}
	
	@RequestMapping(value = "/editbasisIncome", method = RequestMethod.POST)
	public String editBasisIncome(Principal principal, @ModelAttribute("basisIncome") BasisIncome basisIncome, @RequestParam Integer id) throws UnsupportedEncodingException {
		basisIncome.setUserid(Integer.parseInt(principal.getName()));
		System.out.println(basisIncome.getBasis()+basisIncome.getId()+"_________"+id);
		basisIncomeService.save(basisIncome);
		return "redirect:/basisIncome";
	}
	@RequestMapping(value = "/editbasisIncome", method = RequestMethod.GET)
	public String upBasisIncome(Model model, Principal principal, @RequestParam Integer id) {
	model.addAttribute("handelist", basisIncomeService.findAllByUserid(principal));
	model.addAttribute("basisIncome", basisIncomeService.findById(id));
	model.addAttribute("document", Document.BASISINCOME);
	return "/basisIncome";}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String deleteBasisIncome(@RequestParam Integer id, @RequestParam String document) {

		switch (Document.valueOf(document)) {
		case USERS:
			usersService.delete(id);
			return "redirect:/reglist";
		default:
			System.out.println("Error_____for Enum");
			return "redirect:/ ";
		}
	}


}
