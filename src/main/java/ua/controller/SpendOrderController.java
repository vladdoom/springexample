package ua.controller;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ua.dao.BasisSpend;
import ua.dao.Employee;
import ua.dao.SpendOrder;
import ua.service.BasisSpendService;
import ua.service.EmployeeService;
import ua.service.SpendOrderService;
import ua.service.editor.BasisSpendEditor;
import ua.service.editor.EmployeeEditor;
import ua.utility.Document;
import ua.utility.FormatDate;
import ua.validator.SpendOrderValidator;
@Controller
public class SpendOrderController {
	@Autowired
	SpendOrderService spendOrderService;
	@Autowired
	BasisSpendService basisSpendService;
	@Autowired
	EmployeeService employeeService;
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		 binder.setValidator(new SpendOrderValidator(spendOrderService));
		 binder.registerCustomEditor(BasisSpend.class, new BasisSpendEditor(basisSpendService));
			binder.registerCustomEditor(Employee.class, new EmployeeEditor(employeeService));
	}

	@RequestMapping(value = "/spendorder", method = RequestMethod.POST)
	public String createSpendorder(Model model, Principal principal, @Valid @ModelAttribute("spendorder") SpendOrder spendorder,
			BindingResult result) {
		spendorder.setUserid(Integer.parseInt(principal.getName()));
		if (result.hasErrors()) {
			model.addAttribute("handelist", spendOrderService.findAllByUserid(principal));
			model.addAttribute("spendorder", spendorder);
			model.addAttribute("basisSpend", basisSpendService.findAllByUserid(principal));
			model.addAttribute("employees", employeeService.findAllByUserid(principal));
		
			return "/spendorder";
	    }
		spendOrderService.save(spendorder);
		return "redirect:/spendorder";
	}

	@RequestMapping(value = "/spendorder", method = RequestMethod.GET)
	public String handleSpendorder(Model model, Principal principal) {
		if (principal==null){System.out.println("User no autitification___");}else{
		model.addAttribute("handelist", spendOrderService.findAllByUserid(principal));
		SpendOrder spendOrder = new SpendOrder();
		spendOrder.setDates(getDate());
		model.addAttribute("spendorder", spendOrder);
		model.addAttribute("basisSpend", basisSpendService.findAllByUserid(principal));
		model.addAttribute("employees", employeeService.findAllByUserid(principal));
		model.addAttribute("document", Document.SPENDORDER);}
		return "/spendorder";
	}
	@RequestMapping(value = "/editspendorder", method = RequestMethod.POST)
	public String editSpendorder(Model model, Principal principal, @Valid @ModelAttribute("spendorder") SpendOrder spendorder,
			BindingResult result) {
		spendorder.setUserid(Integer.parseInt(principal.getName()));
		if (result.hasErrors()) {
			model.addAttribute("handelist", spendOrderService.findAllByUserid(principal));
			model.addAttribute("spendorder", spendorder);
			model.addAttribute("basisSpend", basisSpendService.findAllByUserid(principal));
			model.addAttribute("employees", employeeService.findAllByUserid(principal));
			
	        return "/spendorder";
	    }
		spendOrderService.update(spendorder);
		return "redirect:/spendorder";
	}

	@RequestMapping(value = "/editspendorder", method = RequestMethod.GET)
	public String viewSpendorder(Model model, Principal principal, @RequestParam("id") int id) {
		model.addAttribute("handelist", spendOrderService.findAllByUserid(principal));
		SpendOrder spendOrder =  spendOrderService.findById(id);
		spendOrder.setSumatransient(String.valueOf(spendOrder.getSuma()));
		model.addAttribute("spendorder", spendOrder);
		model.addAttribute("basisSpend", basisSpendService.findAllByUserid(principal));
		model.addAttribute("employees", employeeService.findAllByUserid(principal));
		return "spendorder";
	}
	public String getDate(){
		 Date date = new Date();
		  SimpleDateFormat formater = new SimpleDateFormat("dd.MM.yyyy");
		 return formater.format(date);
	}
}
