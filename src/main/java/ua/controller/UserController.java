package ua.controller;


import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import ua.dao.Users;
import ua.service.UsersService;
import ua.validator.UserValidator;

@Controller
public class UserController {
	@Autowired
	UsersService usersService;
	
	@InitBinder
	protected void initBinder(WebDataBinder binder){
		
		binder.setValidator(new UserValidator(usersService));
	}
	
	@RequestMapping("/")
	public String handleFirst( Model model) {
		model.addAttribute("newUser", new Users());
		return "homepage";
	}
	@RequestMapping("/homepage")
	public String handleFirstM(Model model) {
		return "redirect:/";
	}
	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public String handleRegistration( @Valid @ModelAttribute("newUser") Users newUser, 
			BindingResult br) {
				if (br.hasErrors()){
			return "/homepage";
		}
		usersService.save( newUser);
		return "redirect:/homepage";
	}
	
	
	
}
