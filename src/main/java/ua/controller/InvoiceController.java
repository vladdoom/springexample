package ua.controller;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ua.dao.Firm;
import ua.dao.Invoice;
import ua.service.FirmEditor;
import ua.service.FirmService;
import ua.service.InvoiceService;
import ua.utility.Document;
import ua.utility.FormatDate;
import ua.validator.InvoiceValidator;
@Controller
public class InvoiceController {
	@Autowired
	FirmService firmService;
	@Autowired
	InvoiceService invoiceService;
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		 binder.setValidator(new InvoiceValidator(invoiceService));	
		 binder.registerCustomEditor(Firm.class, new FirmEditor(firmService));
	}
	

	@RequestMapping(value = "/invoice", method = RequestMethod.POST)
	public String createInvoice(Model model, Principal principal,@Valid @ModelAttribute("invoice") Invoice invoice, BindingResult result) {
		invoice.setUserid(Integer.parseInt(principal.getName()));
		if (result.hasErrors()) {
			model.addAttribute("handelist", invoiceService.findAllByUserid(principal));
			model.addAttribute("invoice", invoice);
			model.addAttribute("listfirm", firmService.findAllByUserid(principal));
	        return "/invoice";
	    }
		
		invoiceService.save(invoice);
		return "redirect:/invoice";
	}

	@RequestMapping(value = "/invoice", method = RequestMethod.GET)
	public String handleInvoice(Model model, Principal principal) {
		if (principal==null){System.out.println("User no autitification___");}else{
		model.addAttribute("handelist", invoiceService.findAllByUserid(principal));
		Invoice invoice =new Invoice();
		invoice.setDate(getDate());
		model.addAttribute("invoice", invoice);
		model.addAttribute("listfirm", firmService.findAllByUserid(principal));
		model.addAttribute("document", Document.INVOICE);}
		return "invoice";
	}
	@RequestMapping(value = "/editinvoice", method = RequestMethod.POST)
	public String updateInvoice(Model model, Principal principal,@Valid @ModelAttribute("invoice") Invoice invoice, BindingResult result) {
		invoice.setUserid(Integer.parseInt(principal.getName()));
		if (result.hasErrors()) {
			model.addAttribute("handelist", invoiceService.findAllByUserid(principal));
			model.addAttribute("invoice", invoice);
			model.addAttribute("listfirm", firmService.findAllByUserid(principal));
			return "/invoice";
	    }
		invoiceService.update(invoice);
		return "redirect:/invoice";
	}

	@RequestMapping(value = "/editinvoice", method = RequestMethod.GET)
	public String viewInvoice(Model model, Principal principal, @RequestParam("id") int id) {
		model.addAttribute("handelist", invoiceService.findAllByUserid(principal));
		Invoice invoice =  invoiceService.findById(id);
		invoice.setSumatransient(String.valueOf(invoice.getSumainv()));
		
		invoice.setPdvtransient(String.valueOf(invoice.getPdv()));
		model.addAttribute("invoice",invoice);
		model.addAttribute("listfirm", firmService.findAllByUserid(principal));
		model.addAttribute("document", Document.INVOICE);
		return "/invoice";
	}
	public String getDate(){
		 Date date = new Date();
		  SimpleDateFormat formater = new SimpleDateFormat("dd.MM.yyyy");
		 return formater.format(date);
	}
}
