package ua.dao;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
//import javax.validation.Valid;

@Entity
public class Firm {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private int userid;
	@Column (length=50)
	private String name;
	@OneToMany(cascade = CascadeType.ALL, mappedBy="firmName")
	private List<Payment> payments;
	@OneToMany(cascade = CascadeType.ALL, mappedBy="firmName")
	private List<Invoice> invoices;
	
	@Column(length=25)
	private String bill;
	
	@Column(length=15)
	private String edrpou;
	
	@Column(length=15)
	private String mfo;

	public Firm() {
	}

	/**
	 * @param id
	 * @param userid
	 * @param name
	 * @param providers
	 * @param payments
	 * @param invoices
	 * @param bill
	 * @param edrpou
	 * @param mfo
	 */
	public Firm(int id, int userid, String name,  List<Payment> payments,
			List<Invoice> invoices, String bill, String edrpou, String mfo) {
		this.id = id;
		this.userid = userid;
		this.name = name;
		this.payments = payments;
		this.invoices = invoices;
		this.bill = bill;
		this.edrpou = edrpou;
		this.mfo = mfo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Payment> getPayments() {
		return payments;
	}

	public void setPayments(List<Payment> payments) {
		this.payments = payments;
	}

	public List<Invoice> getInvoices() {
		return invoices;
	}

	public void setInvoices(List<Invoice> invoices) {
		this.invoices = invoices;
	}

	public String getBill() {
		return bill;
	}

	public void setBill(String bill) {
		this.bill = bill;
	}

	public String getEdrpou() {
		return edrpou;
	}

	public void setEdrpou(String edrpou) {
		this.edrpou = edrpou;
	}

	public String getMfo() {
		return mfo;
	}

	public void setMfo(String mfo) {
		this.mfo = mfo;
	}

	@Override
	public String toString() {
		return "Firm [id=" + id + ", userid=" + userid + ", name=" + name + "]";
	}

	

	}
