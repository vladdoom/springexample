package ua.dao;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
public class Payment {
	@Id
	private int id;
	
	private int userid;
	
	@Column(length=15)
	private String number;
	
	@Column (length=10)
	private String date;
	
	@ManyToOne 
	@JoinColumn(name = "firmName")
	private Firm firmName;
	
	private BigDecimal sumapay;
	
	@Transient
	private String sumatransient;
	
	@Column (length=50)
	private String notes;

	@MapsId 
    @OneToOne
    @JoinColumn(name = "id")    
    @PrimaryKeyJoinColumn
    @Fetch(FetchMode.JOIN)
	private Bank bank;
	
	@MapsId 
    @OneToOne
    @JoinColumn(name = "id")    
    @PrimaryKeyJoinColumn
    @Fetch(FetchMode.JOIN)
	private Provider provider;	
	
	public Payment() {
		
	}
	public Payment(int id, int userid, String number, String date, Firm firmName, BigDecimal sumapay, String notes,
			Bank bank, Provider provider) {
		this.id = id;
		this.userid = userid;
		this.number = number;
		this.date = date;
		this.firmName = firmName;
		this.sumapay = sumapay;
		this.notes = notes;
		this.bank = bank;
		this.provider = provider;
	}



	public String getSumatransient() {
		return sumatransient;
	}
	public void setSumatransient(String sumatransient) {
		this.sumatransient = sumatransient;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Firm getFirmName() {
		return firmName;
	}
	public void setFirmName(Firm firmName) {
		this.firmName = firmName;
	}
	public BigDecimal getSumapay() {
		return sumapay;
	}
	public void setSumapay(BigDecimal sumapay) {
		this.sumapay = sumapay;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Bank getBank() {
		return bank;
	}
	public void setBank(Bank bank) {
		this.bank = bank;
	}
	public Provider getProvider() {
		return provider;
	}
	public void setProvider(Provider provider) {
		this.provider = provider;
	}
	@Override
	public String toString() {
		return "Payment [id=" + id + ", userid=" + userid + ", sumapay=" + sumapay + ", notes=" + notes + "]";
	}
	
	}
