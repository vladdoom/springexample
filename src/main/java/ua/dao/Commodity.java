package ua.dao;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
public class Commodity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private int userid;
	
	@OneToOne 
	@PrimaryKeyJoinColumn
	private Invoice invoice;
	
	private BigDecimal balance;

	/**
	 * @param id
	 * @param userid
	 * @param invoice
	 * @param balance
	 */
	public Commodity(){
		
	}
	public Commodity(int id, int userid, Invoice invoice, BigDecimal balance) {
		this.id = id;
		this.userid = userid;
		this.invoice = invoice;
		this.balance = balance;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public Invoice getInvoice() {
		return invoice;
	}
	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	@Override
	public String toString() {
		return "Commodity [id=" + id + ", userid=" + userid + ", balance=" + balance + "]";
	}
	
	
}
