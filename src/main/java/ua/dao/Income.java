package ua.dao;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
public class Income {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private int userid;
	
	@OneToOne 
	@PrimaryKeyJoinColumn
	private IncomeOrder incomeOrder;

	private BigDecimal balance;
	
	public Income(){
		
	}

	public Income(int id, int userid, IncomeOrder incomeOrder, BigDecimal balance) {
		this.id = id;
		this.userid = userid;
		this.incomeOrder = incomeOrder;
		this.balance = balance;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public IncomeOrder getIncomeOrder() {
		return incomeOrder;
	}

	public void setIncomeOrder(IncomeOrder incomeOrder) {
		this.incomeOrder = incomeOrder;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "Income [id=" + id + ", userid=" + userid + ", balance=" + balance + "]";
	}
	
	}
