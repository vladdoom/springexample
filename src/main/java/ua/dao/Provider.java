package ua.dao;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
public class Provider {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private int userid;

	@OneToOne 
	@PrimaryKeyJoinColumn
	private Payment payment;
	
	@OneToOne 
	@PrimaryKeyJoinColumn
	private Invoice invoice;
	
	private BigDecimal balance;

	/**
	 * @param id
	 * @param userid
	 * @param firmName
	 * @param payment
	 * @param invoice
	 * @param balance
	 */
	public Provider(){
		
	}
	public Provider(int id, int userid,  Payment payment, Invoice invoice, BigDecimal balance) {
		this.id = id;
		this.userid = userid;
		this.payment = payment;
		this.invoice = invoice;
		this.balance = balance;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public Payment getPayment() {
		return payment;
	}
	public void setPayment(Payment payment) {
		this.payment = payment;
	}
	public Invoice getInvoice() {
		return invoice;
	}
	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	@Override
	public String toString() {
		return "Provider [id=" + id + ", userid=" + userid + ", balance=" + balance + "]";
	}

	
	
}
