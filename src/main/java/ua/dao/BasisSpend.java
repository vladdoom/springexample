package ua.dao;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
//import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
@Entity
public class BasisSpend {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private int userid;
	
	@Column (length=50)
	private String basis;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="basis")
	private List<SpendOrder> spendorders;


	public BasisSpend(){
		
	}
	/**
	 * @param id
	 * @param userid
	 * @param basis
	 * @param spendorders
	 */
	public BasisSpend(int id, int userid, String basis, List<SpendOrder> spendorders) {
		this.id = id;
		this.userid = userid;
		this.basis = basis;
		this.spendorders = spendorders;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getBasis() {
		return basis;
	}
	public void setBasis(String basis) {
		this.basis = basis;
	}
	public List<SpendOrder> getSpendorders() {
		return spendorders;
	}
	public void setSpendorders(List<SpendOrder> spendorders) {
		this.spendorders = spendorders;
	}
	@Override
	public String toString() {
		return  basis;
	}
	}
