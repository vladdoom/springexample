package ua.dao;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Rezult {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private int userid;
	
	private BigDecimal income;
	
	private BigDecimal till;
	
	private BigDecimal bank;
	
	private BigDecimal provider;
	
	private BigDecimal commodity;

	/**
	 * @param id
	 * @param userid
	 * @param income
	 * @param till
	 * @param bank
	 * @param provider
	 * @param commodity
	 */
	public Rezult(){
		
	}
	public Rezult(int id, int userid, BigDecimal income, BigDecimal till, BigDecimal bank, BigDecimal provider,
			BigDecimal commodity) {
		this.id = id;
		this.userid = userid;
		this.income = income;
		this.till = till;
		this.bank = bank;
		this.provider = provider;
		this.commodity = commodity;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public BigDecimal getIncome() {
		return income;
	}
	public void setIncome(BigDecimal income) {
		this.income = income;
	}
	public BigDecimal getTill() {
		return till;
	}
	public void setTill(BigDecimal till) {
		this.till = till;
	}
	public BigDecimal getBank() {
		return bank;
	}
	public void setBank(BigDecimal bank) {
		this.bank = bank;
	}
	public BigDecimal getProvider() {
		return provider;
	}
	public void setProvider(BigDecimal provider) {
		this.provider = provider;
	}
	public BigDecimal getCommodity() {
		return commodity;
	}
	public void setCommodity(BigDecimal commodity) {
		this.commodity = commodity;
	}
	@Override
	public String toString() {
		return "Rezult [id=" + id + ", userid=" + userid + ", income=" + income + ", till=" + till + ", bank=" + bank
				+ ", provider=" + provider + ", commodity=" + commodity + "]";
	}
	
	
}
