package ua.dao;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


@Entity
public class SpendOrder {
	@Id
	private int id;
	
	private int userid;
	
	@Column (length=10)
	private String dates;
	
	@Column (length=15)
	private String number;
	
	@ManyToOne
	@JoinColumn(name = "basis")
	private BasisSpend basis;
	
	private BigDecimal suma;
	
	@Transient
	private String sumatransient;
	
	@Column (length=15)
	private String reciev;
	
	@ManyToOne
	@JoinColumn(name = "cashier")
	private Employee cashier;

	
	@MapsId 
    @OneToOne
    @JoinColumn(name = "id")    
    @PrimaryKeyJoinColumn
    @Fetch(FetchMode.JOIN)
	private Till till;
	
	@MapsId 
    @OneToOne
    @JoinColumn(name = "id")    
    @PrimaryKeyJoinColumn
    @Fetch(FetchMode.JOIN)
	private Bank bank;

	/**
	 * @param id
	 * @param userid
	 * @param dates
	 * @param number
	 * @param basis
	 * @param suma
	 * @param reciev
	 * @param cashier
	 * @param till
	 * @param bank
	 */
	public SpendOrder(){
		
	}
	public SpendOrder(int id, int userid, String dates, String number, BasisSpend basis, BigDecimal suma, String reciev,
			Employee cashier, Till till, Bank bank) {
		this.id = id;
		this.userid = userid;
		this.dates = dates;
		this.number = number;
		this.basis = basis;
		this.suma = suma;
		this.reciev = reciev;
		this.cashier = cashier;
		this.till = till;
		this.bank = bank;
	}

	public String getSumatransient() {
		return sumatransient;
	}
	public void setSumatransient(String sumatransient) {
		this.sumatransient = sumatransient;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getDates() {
		return dates;
	}
	public void setDates(String dates) {
		this.dates = dates;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public BasisSpend getBasis() {
		return basis;
	}
	public void setBasis(BasisSpend basis) {
		this.basis = basis;
	}
	public BigDecimal getSuma() {
		return suma;
	}
	public void setSuma(BigDecimal suma) {
		this.suma = suma;
	}
	public String getReciev() {
		return reciev;
	}
	public void setReciev(String reciev) {
		this.reciev = reciev;
	}
	public Employee getCashier() {
		return cashier;
	}
	public void setCashier(Employee cashier) {
		this.cashier = cashier;
	}
	public Till getTill() {
		return till;
	}
	public void setTill(Till till) {
		this.till = till;
	}
	public Bank getBank() {
		return bank;
	}
	public void setBank(Bank bank) {
		this.bank = bank;
	}
	@Override
	public String toString() {
		return "SpendOrder [id=" + id + ", userid=" + userid + ", suma=" + suma + "]";
	}
	

	
		}
