package ua.dao;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
//import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
@Entity
public class BasisIncome {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private int userid;
	
	@Column (length=50)
	private String basis;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="basis")
	private List<IncomeOrder> incomeorders;


	public BasisIncome(){
		
	}


	/**
	 * @param id
	 * @param userid
	 * @param basis
	 * @param incomeorders
	 */
	public BasisIncome(int id, int userid, String basis, List<IncomeOrder> incomeorders) {
		this.id = id;
		this.userid = userid;
		this.basis = basis;
		this.incomeorders = incomeorders;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getUserid() {
		return userid;
	}


	public void setUserid(int userid) {
		this.userid = userid;
	}


	public String getBasis() {
		return basis;
	}


	public void setBasis(String basis) {
		this.basis = basis;
	}


	public List<IncomeOrder> getIncomeorders() {
		return incomeorders;
	}


	public void setIncomeorders(List<IncomeOrder> incomeorders) {
		this.incomeorders = incomeorders;
	}


	@Override
	public String toString() {
		return "BasisIncome [id=" + id + ", userid=" + userid + ", basis=" + basis + "]";
	}


	





}
