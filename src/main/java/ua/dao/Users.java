package ua.dao;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Users {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column (length=50)
	private String username;
	@Column (length=50)
	private String emaile;
	
	private String password;
	@Column (length=15)
	private String ROLE;
	@Column (length=15)
	private String lastActivity;
	
	@Column (length=2)
	private String language;
	

	public Users() {

	}


	/**
	 * @param id
	 * @param username
	 * @param emaile
	 * @param password
	 * @param rOLE
	 * @param lastActivity
	 * @param language
	 */
	public Users(int id, String username, String emaile, String password, String rOLE, String lastActivity,
			String language) {
		this.id = id;
		this.username = username;
		this.emaile = emaile;
		this.password = password;
		ROLE = rOLE;
		this.lastActivity = lastActivity;
		this.language = language;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getEmaile() {
		return emaile;
	}


	public void setEmaile(String emaile) {
		this.emaile = emaile;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getROLE() {
		return ROLE;
	}


	public void setROLE(String rOLE) {
		ROLE = rOLE;
	}


	public String getLastActivity() {
		return lastActivity;
	}


	public void setLastActivity(String lastActivity) {
		this.lastActivity = lastActivity;
	}


	public String getLanguage() {
		return language;
	}


	public void setLanguage(String language) {
		this.language = language;
	}


	@Override
	public String toString() {
		return "Users [id=" + id + ", username=" + username + ", emaile=" + emaile + ", password=" + password
				+ ", ROLE=" + ROLE + ", lastActivity=" + lastActivity + ", language=" + language + "]";
	}

	

}
