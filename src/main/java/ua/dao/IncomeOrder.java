package ua.dao;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
public class IncomeOrder {
	@Id
	private int id;

	private int userid;

	@Column(length = 10)
	private String dates;

	@Column(length = 15)
	private String number;

	@ManyToOne
	@JoinColumn(name = "basis")
	private BasisIncome basis;

	private BigDecimal suma;
	
	@Transient
	private String sumatransient;
	
	@Column(length = 15)
	private String accept;

	@ManyToOne
	@JoinColumn(name = "cashier")
	private Employee cashier;

	@MapsId 
    @OneToOne
    @JoinColumn(name = "id")    
    @PrimaryKeyJoinColumn
    @Fetch(FetchMode.JOIN)
	private Income income;

	@MapsId 
    @OneToOne
    @JoinColumn(name = "id")    
    @PrimaryKeyJoinColumn
    @Fetch(FetchMode.JOIN)
	private Till till;
	
	
	/**
	 * @param id
	 * @param userid
	 * @param dates
	 * @param number
	 * @param basis
	 * @param suma
	 * @param accept
	 * @param cashier
	 * @param accountable
	 * @param till
	 */
	public IncomeOrder(){
		
	}
	public IncomeOrder(int id, int userid, String dates, String number, BasisIncome basis, BigDecimal suma, String accept,
			Employee cashier, Income income, Till till) {
		this.id = id;
		this.userid = userid;
		this.dates = dates;
		this.number = number;
		this.basis = basis;
		this.suma = suma;
		this.accept = accept;
		this.cashier = cashier;
		this.income = income;
		this.till = till;
	}


	public String getSumatransient() {
		return sumatransient;
	}
	public void setSumatransient(String sumatransient) {
		this.sumatransient = sumatransient;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getDates() {
		return dates;
	}
	public void setDates(String dates) {
		this.dates = dates;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public BasisIncome getBasis() {
		return basis;
	}
	public void setBasis(BasisIncome basis) {
		this.basis = basis;
	}
	public BigDecimal getSuma() {
		return suma;
	}
	public void setSuma(BigDecimal suma) {
		this.suma = suma;
	}
	public String getAccept() {
		return accept;
	}
	public void setAccept(String accept) {
		this.accept = accept;
	}
	public Employee getCashier() {
		return cashier;
	}
	public void setCashier(Employee cashier) {
		this.cashier = cashier;
	}
	public Income getIncome() {
		return income;
	}
	public void setIncome(Income income) {
		this.income = income;
	}
	public Till getTill() {
		return till;
	}
	public void setTill(Till till) {
		this.till = till;
	}
	@Override
	public String toString() {
		return "IncomeOrder [id=" + id + ", userid=" + userid + ", dates=" + dates + ", number=" + number + ", suma="
				+ suma + ", accept=" + accept + "]";
	}

}
