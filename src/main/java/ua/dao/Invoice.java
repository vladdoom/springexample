package ua.dao;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
public class Invoice {
	@Id
	private int id;
	
	private int userid;

	@ManyToOne
	@JoinColumn(name = "firmName")
	private Firm firmName;
	
	@Column(length=25)
	private String number;
	
	@Column (length=10)
	private String date;
	
	private BigDecimal sumainv;
	
	@Transient
	private String sumatransient;
	
	private BigDecimal pdv; 
	
	@Transient
	private String pdvtransient;
	
	@MapsId 
    @OneToOne
    @JoinColumn(name = "id")    
    @PrimaryKeyJoinColumn
    @Fetch(FetchMode.JOIN)
	private Provider provider;
	
	@MapsId 
    @OneToOne
    @JoinColumn(name = "id")    
    @PrimaryKeyJoinColumn
    @Fetch(FetchMode.JOIN)
	private Commodity commodity;
	
	/**
	 * @param id
	 * @param userid
	 * @param firmName
	 * @param number
	 * @param date
	 * @param sumainv
	 * @param pdv
	 * @param provider
	 * @param commodity
	 */
	public Invoice(){
		
	}
	public Invoice(int id, int userid, Firm firmName, String number, String date, BigDecimal sumainv, BigDecimal pdv,
			Provider provider, Commodity commodity) {
		this.id = id;
		this.userid = userid;
		this.firmName = firmName;
		this.number = number;
		this.date = date;
		this.sumainv = sumainv;
		this.pdv = pdv;
		this.provider = provider;
		this.commodity = commodity;
	}
	
	public String getSumatransient() {
		return sumatransient;
	}
	public void setSumatransient(String sumatransient) {
		this.sumatransient = sumatransient;
	}
	public String getPdvtransient() {
		return pdvtransient;
	}
	public void setPdvtransient(String pdvtransient) {
		this.pdvtransient = pdvtransient;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public Firm getFirmName() {
		return firmName;
	}
	public void setFirmName(Firm firmName) {
		this.firmName = firmName;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public BigDecimal getSumainv() {
		return sumainv;
	}
	public void setSumainv(BigDecimal sumainv) {
		this.sumainv = sumainv;
	}
	public BigDecimal getPdv() {
		return pdv;
	}
	public void setPdv(BigDecimal pdv) {
		this.pdv = pdv;
	}
	public Provider getProvider() {
		return provider;
	}
	public void setProvider(Provider provider) {
		this.provider = provider;
	}
	public Commodity getCommodity() {
		return commodity;
	}
	public void setCommodity(Commodity commodity) {
		this.commodity = commodity;
	}
	@Override
	public String toString() {
		return "Invoice [id=" + id + ", userid=" + userid + ", number=" + number + ", date=" + date + ", sumainv="
				+ sumainv + ", pdv=" + pdv + "]";
	}
	
	}
