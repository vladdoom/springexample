package ua.dao;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity

public class Till {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private int userid;
	
	@OneToOne 
	@PrimaryKeyJoinColumn
	private IncomeOrder incomeOrder;
	
	@OneToOne
	@PrimaryKeyJoinColumn
	private SpendOrder spendOrder;
	
	private BigDecimal balance;

	/**
	 * @param id
	 * @param userid
	 * @param incomeOrder
	 * @param spendOrder
	 * @param balance
	 */
	public Till(){
		
	}
	public Till(int id, int userid, IncomeOrder incomeOrder, SpendOrder spendOrder, BigDecimal balance) {
		this.id = id;
		this.userid = userid;
		this.incomeOrder = incomeOrder;
		this.spendOrder = spendOrder;
		this.balance = balance;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public IncomeOrder getIncomeOrder() {
		return incomeOrder;
	}
	public void setIncomeOrder(IncomeOrder incomeOrder) {
		this.incomeOrder = incomeOrder;
	}
	public SpendOrder getSpendOrder() {
		return spendOrder;
	}
	public void setSpendOrder(SpendOrder spendOrder) {
		this.spendOrder = spendOrder;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	@Override
	public String toString() {
		return "Till [id=" + id + ", userid=" + userid + ", balance=" + balance + "]";
	}

	
	}
