package ua.dao;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
public class Bank {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private int userid;
	
	@OneToOne 
	@PrimaryKeyJoinColumn
	private SpendOrder spendOrder;
	
	@OneToOne 
	@PrimaryKeyJoinColumn
	private Payment payment;
	
	private BigDecimal balance;

	/**
	 * @param id
	 * @param userid
	 * @param spendOrder
	 * @param payment
	 * @param balance
	 */
	public Bank(){
		
	}
	public Bank(int id, int userid, SpendOrder spendOrder, Payment payment, BigDecimal balance) {
		this.id = id;
		this.userid = userid;
		this.spendOrder = spendOrder;
		this.payment = payment;
		this.balance = balance;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public SpendOrder getSpendOrder() {
		return spendOrder;
	}
	public void setSpendOrder(SpendOrder spendOrder) {
		this.spendOrder = spendOrder;
	}
	public Payment getPayment() {
		return payment;
	}
	public void setPayment(Payment payment) {
		this.payment = payment;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	@Override
	public String toString() {
		return "Bank [id=" + id + ", userid=" + userid + ", balance=" + balance + "]";
	}

		
}
