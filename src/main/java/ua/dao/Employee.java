package ua.dao;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private int userid;
	
	@Column (length=50)
	private String name;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="cashier")
	private List<IncomeOrder> incomeorders;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="cashier")
	private List<SpendOrder> spendorders;
	/**
	 * @param id
	 * @param userid
	 * @param name
	 * @param incomeorders
	 * @param spendorders
	 */
	public Employee(){
		
	}
	public Employee(int id, int userid, String name, List<IncomeOrder> incomeorders, List<SpendOrder> spendorders) {
		this.id = id;
		this.userid = userid;
		this.name = name;
		this.incomeorders = incomeorders;
		this.spendorders = spendorders;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<IncomeOrder> getIncomeorders() {
		return incomeorders;
	}
	public void setIncomeorders(List<IncomeOrder> incomeorders) {
		this.incomeorders = incomeorders;
	}
	public List<SpendOrder> getSpendorders() {
		return spendorders;
	}
	public void setSpendorders(List<SpendOrder> spendorders) {
		this.spendorders = spendorders;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", userid=" + userid + ", name=" + name + "]";
	}
	
		
}
