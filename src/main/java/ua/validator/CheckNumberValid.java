package ua.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckNumberValid {
	private String number;
	
	
	public CheckNumberValid(String number) {
		this.number = number;
	}


	public boolean checkNumber(){  
	       Pattern p = Pattern.compile("^[0-9]{1,9}"+"."+"[0-9]{1,2}$"); 
	        Matcher m = p.matcher(number);
	        System.out.println(number);
	        System.out.println(!m.matches());
	         return !m.matches();  
	    }
	
}
