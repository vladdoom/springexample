package ua.validator;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ua.dao.Payment;
import ua.service.PaymentService;

public class PaymentValidator implements Validator {
	private final PaymentService paymentService;
	private CheckNumberValid checkNumberValid;

	/**
	 * @param paymentService
	 */
	public PaymentValidator(PaymentService paymentService) {
		this.paymentService = paymentService;
	}

	public boolean supports(Class<?> arg0) {
		return Payment.class.equals(arg0);
	}

	public void validate(Object target, Errors errors) {
		System.out.println("Validator payment_____________");
		Payment payment = (Payment) target;
		if (payment.getNotes().length() > 50) {
			payment.setNotes(payment.getNotes().substring(0, 49));
		}
		if (payment.getNumber().length() > 15) {
			payment.setNumber(payment.getNumber().substring(0, 14));
		}
		if (payment.getDate().length()>10) {
			payment.setDate(payment.getDate().substring(0,9));
		}
		payment.setSumatransient(payment.getSumatransient().replace(',', '.'));
		checkNumberValid = new CheckNumberValid(payment.getSumatransient());
		if (checkNumberValid.checkNumber()) {
			errors.rejectValue("sumatransient", "payment.sumatransient", "Rewrite a numeric");
		} else {
			payment.setSumapay(
					BigDecimal.valueOf(Double.valueOf(payment.getSumatransient())).setScale(2, RoundingMode.HALF_UP));
		}
	}

}
