package ua.validator;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ua.dao.Invoice;
import ua.service.InvoiceService;

public class InvoiceValidator implements Validator {
	private final InvoiceService invoiceService;
	private CheckNumberValid checkNumberValid;

	public InvoiceValidator(InvoiceService invoiceService) {
		this.invoiceService = invoiceService;
	}

	public boolean supports(Class<?> arg0) {
		return Invoice.class.equals(arg0);
	}

	public void validate(Object target, Errors errors) {
		Invoice invoice =(Invoice)target;
		invoice.setSumatransient(invoice.getSumatransient().replace(',', '.'));
		invoice.setPdvtransient(invoice.getPdvtransient().replace(',', '.'));
		checkNumberValid = new CheckNumberValid(invoice.getSumatransient());
		 if (checkNumberValid.checkNumber() ){
			errors.rejectValue("sumatransient", "invoice.sumatransient", "Rewrite a numeric");
		}			else{
			invoice.setSumainv(BigDecimal.valueOf(Double.valueOf(invoice.getSumatransient())).setScale(2, RoundingMode.HALF_UP));
		}	
		 checkNumberValid = new CheckNumberValid(invoice.getPdvtransient());
		 if (checkNumberValid.checkNumber() ){
					errors.rejectValue("pdvtransient", "invoice.pdvtransient", "Rewrite a numeric");
				}else{
					invoice.setPdv(BigDecimal.valueOf(Double.valueOf(invoice.getPdvtransient())).setScale(2, RoundingMode.HALF_UP));
				}
		if(invoice.getNumber().length()>25){
			invoice.setNumber(invoice.getNumber().substring(0, 24));
		}				
		if (invoice.getDate().length()>10){
			invoice.setDate(invoice.getDate().substring(0,9));
		}
	}

}
