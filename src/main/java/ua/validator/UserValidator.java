package ua.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ua.dao.Users;
import ua.service.UsersService;

public class UserValidator implements Validator {
	private final UsersService usersService;

	/**
	 * @param usersService
	 */
	public UserValidator(UsersService usersService) {
		this.usersService = usersService;
	}

	public boolean supports(Class<?> clazz) {
		return Users.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {
		Users users = (Users) target;
		if (usersService.findByUsername(users.getUsername()) != null) {
			errors.rejectValue("username", "users.username", "Already exists");
		}
		if (users.getPassword().isEmpty()) {
			errors.rejectValue("password", "users.password", "Can not by empty");
		}
	}

}
