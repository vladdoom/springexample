package ua.validator;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ua.dao.SpendOrder;
import ua.service.SpendOrderService;

public class SpendOrderValidator implements Validator{
	private final SpendOrderService spendOrderService;
	private CheckNumberValid checkNumberValid;
	
	public SpendOrderValidator(SpendOrderService spendOrderService) {
		this.spendOrderService = spendOrderService;
	}

	public boolean supports(Class<?> arg0) {
		return SpendOrder.class.equals(arg0);
	}

	public void validate(Object target, Errors errors) {
		SpendOrder spendOrder= (SpendOrder)target;
		spendOrder.setSumatransient(spendOrder.getSumatransient().replace(',', '.'));
		checkNumberValid = new CheckNumberValid(spendOrder.getSumatransient());
		 if (checkNumberValid.checkNumber() ){
			errors.rejectValue("sumatransient", "spendorder.sumatransient", "Rewrite a numeric");
		}else{spendOrder.setSuma(BigDecimal.valueOf(Double.valueOf(spendOrder.getSumatransient())).setScale(2, RoundingMode.HALF_UP));
		}
		if (spendOrder.getNumber().length()>15){
			spendOrder.setNumber(spendOrder.getNumber().substring(0,14));
		}
			
		if(spendOrder.getReciev().length()>15){
			spendOrder.setReciev(spendOrder.getReciev().substring(0, 14));
		}
		if(spendOrder.getDates().length()>10){
			spendOrder.setDates(spendOrder.getDates().substring(0,9));
		}
	}

}
