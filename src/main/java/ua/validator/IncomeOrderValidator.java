package ua.validator;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ua.dao.IncomeOrder;
import ua.service.IncomeOrderService;

public class IncomeOrderValidator implements Validator{
	private final IncomeOrderService incomeOrderService;
	private CheckNumberValid checkNumberValid;
	
	public IncomeOrderValidator(IncomeOrderService incomeOrderService) {
		this.incomeOrderService = incomeOrderService;
	}

	public boolean supports(Class<?> arg0) {	
		return IncomeOrder.class.equals(arg0);
	}

	public void validate(Object target, Errors errors) {
		IncomeOrder incomeOrder = (IncomeOrder)target;
		incomeOrder.setSumatransient(incomeOrder.getSumatransient().replace(',', '.'));
		checkNumberValid = new CheckNumberValid(incomeOrder.getSumatransient());
		System.out.println(incomeOrder.getSumatransient());
		 if (checkNumberValid.checkNumber() ){
			errors.rejectValue("sumatransient", "incomeorder.sumatransient", "Rewrite a numeric");
		}else{incomeOrder.setSuma(BigDecimal.valueOf(Double.valueOf(incomeOrder.getSumatransient())).setScale(2, RoundingMode.HALF_UP));
		}
		if (incomeOrder.getNumber().length()>15){
			incomeOrder.setNumber(incomeOrder.getNumber().substring(0,14));
		}
		if (incomeOrder.getAccept().length()>15){
			incomeOrder.setAccept(incomeOrder.getAccept().substring(0,14));
		}
		if (incomeOrder.getDates().length()>10) {
			incomeOrder.setDates(incomeOrder.getDates().substring(0,9));
		}
	}

}
