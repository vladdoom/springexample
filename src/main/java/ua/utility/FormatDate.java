package ua.utility;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatDate {

	public String getDate(){
		 Date date = new Date();
		  SimpleDateFormat formater = new SimpleDateFormat("dd.MM.yyyy");
		 return formater.format(date);
		
	}
}
