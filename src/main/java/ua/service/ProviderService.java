package ua.service;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ua.dao.Provider;
import ua.repository.ProviderRepository;

@Service
public class ProviderService {
	@Autowired
	ProviderRepository providerRepository;

	

	public void save(Provider provider) {
		providerRepository.save(provider);
	}

	public void delete(int id) {
		providerRepository.delete(id);
	}

	public Iterable<Provider> findAllByUserid(Principal principal) {
		return providerRepository.findAllByUserid(Integer.parseInt(principal.getName()));
	}
}
