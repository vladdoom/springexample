package ua.service;

import java.beans.PropertyEditorSupport;

import ua.dao.Firm;

public class FirmEditor extends PropertyEditorSupport{
	private final FirmService firmService;

	/**
	 * @param firmService
	 */
	public FirmEditor(FirmService firmService) {
		this.firmService = firmService;
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException{
		int id =Integer.parseInt(text); 
				Firm firm = firmService.findById(id);
				setValue(firm);
	}

	}
