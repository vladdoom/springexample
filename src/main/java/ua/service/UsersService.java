package ua.service;

import java.math.BigDecimal;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import ua.dao.Rezult;
import ua.dao.Users;
import ua.repository.RezultRepository;
import ua.repository.UsersRepository;
import ua.service.mail.MailSender;
import ua.utility.FormatDate;

@Service
public class UsersService {
	@Autowired
	private UsersRepository usersRepository;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	RezultRepository rezultRepository;
	@Autowired
	private MailSender mailSender;

	public FormatDate formatDate;

	public void save(String username, String emaile, String password) {
		Users users = new Users();
		users.setUsername(username);
		users.setEmaile(emaile);
		users.setPassword(bCryptPasswordEncoder.encode(password));
		if (username.equals("admin")) {
			users.setROLE("ROLE_ADMIN");
		} else {
			users.setROLE("ROLE_USER");
		}

		usersRepository.save(users);
	}
	public void setLastactivity(Users users){
		String s = (new Date()).toString();
		s = s.substring(0, 10) + s.substring(24, 29);
		users.setLastActivity(s);

	}

	public Iterable<Users> findAllList() {
		return usersRepository.findAll();
	}

	public Users findByName(Principal principal) {
		return usersRepository.findByUsername(principal.getName());
	}

	public Users findByUsername(String login) {
		System.out.println("U____" + usersRepository.findByUsername(login));
		return usersRepository.findByUsername(login);
	}

	public void save(Users users) {
		users.setPassword(bCryptPasswordEncoder.encode(users.getPassword()));
		System.out.println(getDate());
		users.setLastActivity(getDate());
		if (users.getUsername().equals("admin")) {
			users.setROLE("ROLE_ADMIN");
		} else {
			users.setROLE("ROLE_USER");
		}
		usersRepository.save(users);
		saveZeroRezult( users);
		mailSender.sendMail("Please rewrite to confirm email.", users.getEmaile(), "Nano Account Registration");
		}

	public void saveZeroRezult(Users users){
		Rezult rezult = new Rezult();
		rezult.setUserid(users.getId());
		rezult.setBank(BigDecimal.ZERO);
		rezult.setCommodity(BigDecimal.ZERO);
		rezult.setIncome(BigDecimal.ZERO);
		rezult.setProvider(BigDecimal.ZERO);
		rezult.setTill(BigDecimal.ZERO);
		rezultRepository.save(rezult);
	}
	public void delete(Integer id) {
	usersRepository.delete(id);
		
	}
	public String getDate(){
		 Date date = new Date();
		  SimpleDateFormat formater = new SimpleDateFormat("dd.MM.yyyy");
		 return formater.format(date);
		
	}
}
