package ua.service.editor;

import java.beans.PropertyEditorSupport;

import ua.dao.BasisIncome;
import ua.service.BasisIncomeService;

public class BasisIncomeEditor extends PropertyEditorSupport {
	private final BasisIncomeService basisIncomeService;

	/**
	 * @param basisIncomeService
	 */
	public BasisIncomeEditor(BasisIncomeService basisIncomeService) {
		this.basisIncomeService = basisIncomeService;
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		int id = Integer.parseInt(text);
		BasisIncome basisIncome = basisIncomeService.findById(id);
		setValue(basisIncome);
	}
}
