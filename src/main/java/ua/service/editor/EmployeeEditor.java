package ua.service.editor;

import java.beans.PropertyEditorSupport;

import ua.dao.Employee;
import ua.service.EmployeeService;

public class EmployeeEditor extends PropertyEditorSupport{
	private final EmployeeService employeeService;

	/**
	 * @param employeeService
	 */
	public EmployeeEditor(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}
	@Override
	public void setAsText(String text) throws IllegalArgumentException{
		int id =Integer.parseInt(text); 
		Employee employee = employeeService.findById(id); 
		setValue(employee);
	}
}
