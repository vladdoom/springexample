package ua.service.editor;

import java.beans.PropertyEditorSupport;

import ua.dao.BasisSpend;
import ua.service.BasisSpendService;

public class BasisSpendEditor  extends PropertyEditorSupport{
	private final BasisSpendService basisSpendService;
	
	/**
	 * @param basisSpend
	 */
	public BasisSpendEditor(BasisSpendService basisSpendService) {
		this.basisSpendService = basisSpendService;
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException{
		int id =Integer.parseInt(text); 
	BasisSpend basisSpend=basisSpendService.findById(id);
	setValue(basisSpend);
	}
}
