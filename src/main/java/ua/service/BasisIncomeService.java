package ua.service;

import java.io.UnsupportedEncodingException;
import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ua.dao.BasisIncome;
import ua.repository.BasisIncomeRepository;

@Service
public class BasisIncomeService {
	@Autowired
	BasisIncomeRepository basisIncomeRepository;
	@Autowired
	ChangeUtf8 changeUtf8;

	public void save(String basis) {
		BasisIncome basisIncome = new BasisIncome();
		basisIncome.setBasis(basis);
		basisIncomeRepository.save(basisIncome);
	}

	public void save(BasisIncome basisIncome) throws UnsupportedEncodingException {
		System.out.println(basisIncome);
		basisIncome.setBasis(changeUtf8.rewrite(basisIncome.getBasis(), "і"));
		System.out.println(basisIncome);
		basisIncomeRepository.save(basisIncome);
	}

	public void delete(int id) {
		basisIncomeRepository.delete(id);
	}

	public Iterable<BasisIncome> findAll() {
		return basisIncomeRepository.findAll();
	}

	public Iterable<BasisIncome> findAllByUserid(Principal principal) {
		return basisIncomeRepository.findAllByUserid(Integer.parseInt(principal.getName()));
	}

	public BasisIncome findById(Integer id) {
		return basisIncomeRepository.findOne(id);
	}
}
