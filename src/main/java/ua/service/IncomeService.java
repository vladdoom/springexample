package ua.service;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ua.dao.Income;
import ua.repository.IncomeRepository;

@Service
public class IncomeService {
@Autowired
IncomeRepository incomeRepository;

public Iterable<Income> findAllByUserid(Principal principal){
	return incomeRepository.findAllByUserid(Integer.parseInt(principal.getName()));
}
}
