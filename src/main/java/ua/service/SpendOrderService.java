package ua.service;

import java.math.BigDecimal;
import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ua.dao.Bank;
import ua.dao.Rezult;
import ua.dao.SpendOrder;
import ua.dao.Till;
import ua.repository.BankRepository;
import ua.repository.BasisSpendRepository;
import ua.repository.EmployeeRepository;
import ua.repository.RezultRepository;
import ua.repository.SpendOrderRepository;
import ua.repository.TillRepository;

@Service
public class SpendOrderService {
	@Autowired
	SpendOrderRepository spendOrderRepository;
	@Autowired
	BasisSpendRepository basisSpendRepository;
	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	RezultRepository rezultRepository;
	@Autowired
	TillRepository tillRepository;
	@Autowired
	BankRepository bankRepository;

	public void save(SpendOrder spendOrder) {
		spendOrderRepository.save(spendOrder);
		
		Rezult rezult = rezultRepository.findOneByUserid(spendOrder.getUserid());
		rezult.setTill(rezult.getTill().subtract(spendOrder.getSuma()));
		rezult.setBank(rezult.getBank().add(spendOrder.getSuma()));
		rezultRepository.save(rezult);
		
		Till till = new Till ();
		till.setUserid(spendOrder.getUserid());
		till.setIncomeOrder(null);
		till.setSpendOrder(spendOrder);
		till.setBalance(rezult.getTill());
		tillRepository.save(till);
		Bank bank = new Bank();
		bank.setUserid(spendOrder.getUserid());
		bank.setPayment(null);
		bank.setSpendOrder(spendOrder);
		bank.setBalance(rezult.getBank());
		bankRepository.save(bank);
	}

	public void delete(int id) {
		spendOrderRepository.delete(id);
	}

	public Iterable<SpendOrder> findAllByUserid(Principal principal) {
		return spendOrderRepository.findAllByUserid(Integer.parseInt(principal.getName()));
	}

	public SpendOrder findById(Integer id) {
		return spendOrderRepository.findOne(id);
	}

	public void update(SpendOrder spendorder) {
		
		SpendOrder spendOrder2 = findById(spendorder.getId());
		if(spendOrder2.getSuma()==spendOrder2.getSuma()){
			spendOrderRepository.save(spendorder);}else{
			BigDecimal bigDecimal = spendorder.getSuma().subtract(spendOrder2.getSuma());
			spendOrderRepository.save(spendorder);
			
			Rezult rezult = rezultRepository.findOneByUserid(spendorder.getUserid());
			rezult.setIncome(rezult.getBank().add(bigDecimal));
			rezult.setTill(rezult.getTill().add(bigDecimal));
			rezultRepository.save(rezult);
			
			Iterable<Bank> bankIter = bankRepository.findAllByUserid (spendorder.getUserid());
			boolean flag=false;
			for (Bank bank : bankIter) {
				if (!flag && bank.getSpendOrder()!=null && bank.getSpendOrder().getId() == spendorder.getId()){
					flag=true;
				}
				if (flag){bank.setBalance(bank.getBalance().add(bigDecimal));}
			}flag=false;
			bankRepository.save(bankIter);
			Iterable<Till> tillIter = tillRepository.findAllByUserid(spendorder.getUserid());
			for (Till till : tillIter) {
				if(!flag && till.getSpendOrder()!=null &&till.getSpendOrder().getId()==spendorder.getId()){
					flag=true;
				}
				if(flag){till.setBalance(till.getBalance().add(bigDecimal));}
			}
			tillRepository.save(tillIter);

		}

	}
}
