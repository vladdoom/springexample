package ua.service;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ua.dao.Commodity;
import ua.repository.CommodityRepository;
@Service
public class CommodityService {
	@Autowired
	CommodityRepository commodityRepository;

	public Iterable<Commodity> findAllByUserid(Principal principal){
		return commodityRepository.findAllByUserid(Integer.parseInt(principal.getName()));
}
}