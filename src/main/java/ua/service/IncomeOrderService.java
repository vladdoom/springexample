package ua.service;

import java.math.BigDecimal;
import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ua.dao.Income;
import ua.dao.IncomeOrder;
import ua.dao.Rezult;
import ua.dao.Till;
import ua.repository.BasisIncomeRepository;
import ua.repository.EmployeeRepository;
import ua.repository.IncomeOrderRepository;
import ua.repository.IncomeRepository;
import ua.repository.RezultRepository;
import ua.repository.TillRepository;

@Service
public class IncomeOrderService {
	@Autowired
	IncomeOrderRepository incomeOrderRepository;
	@Autowired
	BasisIncomeRepository basisIncomeRepository;
	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	RezultRepository rezultRepository;
	@Autowired
	IncomeRepository incomeRepository;
	@Autowired
	TillRepository tillRepository;

	public void save(IncomeOrder incomeOrder) {
		incomeOrderRepository.save(incomeOrder);
		Rezult rezult = rezultRepository.findOneByUserid(incomeOrder.getUserid());
		rezult.setIncome(rezult.getIncome().add(incomeOrder.getSuma()));
		rezult.setTill(rezult.getTill().add(incomeOrder.getSuma()));
		rezultRepository.save(rezult);
		Income income = new Income();
		income.setUserid(incomeOrder.getUserid());
		income.setIncomeOrder(incomeOrder);
		income.setBalance(rezult.getIncome());
		incomeRepository.save(income);
		Till till = new Till();
		till.setUserid(incomeOrder.getUserid());
		till.setIncomeOrder(incomeOrder);
		till.setSpendOrder(null);
		till.setBalance(rezult.getTill());
		tillRepository.save(till);
	}

	public void delete(int id) {
		incomeOrderRepository.delete(id);
	}

	public Iterable<IncomeOrder> findAllByUserid(Principal principal) {
		return incomeOrderRepository.findAllByUserid(Integer.parseInt(principal.getName()));
	}

	public IncomeOrder findById(Integer id) {
		return incomeOrderRepository.findOne(id);
	}

	public void update(IncomeOrder incomeorder) {
		IncomeOrder incomeOrder2 = findById(incomeorder.getId());
		if (incomeOrder2.getSuma() == incomeorder.getSuma()) {
			incomeOrderRepository.save(incomeorder);
		} else {
			BigDecimal bigDecimal = incomeorder.getSuma().subtract(incomeOrder2.getSuma());
			incomeOrderRepository.save(incomeorder);

			Rezult rezult = rezultRepository.findOneByUserid(incomeorder.getUserid());
			rezult.setIncome(rezult.getIncome().add(bigDecimal));
			rezult.setTill(rezult.getTill().add(bigDecimal));
			rezultRepository.save(rezult);
			System.out.println("Start update income_________");
			Iterable<Income> incomeIter = incomeRepository.findAllByUserid(incomeorder.getUserid());
			boolean flag = false;
			for (Income income : incomeIter) {
				System.out.println(income.getIncomeOrder());
				if (!flag && income.getIncomeOrder().getId()==incomeorder.getId()) {
					flag = true;
					System.out.println(flag);
				}
				if (flag) {
					income.setBalance(income.getBalance().add(bigDecimal));
					System.out.println(income.getBalance());
				}
			}
			flag = false;
			
			incomeRepository.save(incomeIter);
			System.out.println("Start update till");
			Iterable<Till> tillIter = tillRepository.findAllByUserid(incomeorder.getUserid());
			for (Till till : tillIter) {
				System.out.println(till.getIncomeOrder());
				if (!flag && till.getIncomeOrder() != null && till.getIncomeOrder().getId()==incomeorder.getId()) {
					flag = true;
					System.err.println(flag);
				}
				if (flag) {
					till.setBalance(till.getBalance().add(bigDecimal));
					System.out.println(till.getBalance());
				}
			}
			tillRepository.save(tillIter);

		}
	}
}