package ua.service;

import java.math.BigDecimal;
import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ua.dao.Commodity;
import ua.dao.Invoice;
import ua.dao.Provider;
import ua.dao.Rezult;
import ua.repository.CommodityRepository;
import ua.repository.FirmRepository;
import ua.repository.InvoiceRepository;
import ua.repository.ProviderRepository;
import ua.repository.RezultRepository;

@Service
public class InvoiceService {
	@Autowired
	InvoiceRepository invoiceRepository;
	@Autowired
	FirmRepository firmRepository;
	@Autowired
	RezultRepository rezultRepository;
	@Autowired
	ProviderRepository providerRepository;
	@Autowired
	CommodityRepository commodityRepository;
	

	public void save(Invoice invoice) {
		invoiceRepository.save(invoice);
		
		Rezult rezult = rezultRepository.findOneByUserid(invoice.getUserid());
		rezult.setCommodity(rezult.getCommodity().add(invoice.getSumainv()));
		rezult.setProvider(rezult.getProvider().subtract(invoice.getSumainv()));
		rezultRepository.save(rezult);
		
		Provider provider = new Provider();
		provider.setUserid(invoice.getUserid());
		provider.setPayment(null);
		provider.setInvoice(invoice);
		provider.setBalance(rezult.getProvider());
		providerRepository.save(provider);
		
		Commodity commodity = new Commodity();
		commodity.setUserid(invoice.getUserid());
		commodity.setInvoice(invoice);
		commodity.setBalance(rezult.getCommodity());
		commodityRepository.save(commodity);
	}

	public void delete(int id) {
		invoiceRepository.delete(id);
	}

	public Iterable<Invoice> findAllByUserid(Principal principal) {
		return invoiceRepository.findAllByUserid(Integer.parseInt(principal.getName()));
	}

	public Invoice findById(Integer id) {
		return invoiceRepository.findOne(id);
	}

	public void update(Invoice invoice) {
		
		Invoice invoice2 = invoiceRepository.findOne(invoice.getId());
		if(invoice2.getSumainv()==invoice.getSumainv()){
			invoiceRepository.save(invoice);}else{
			BigDecimal bigDecimal = invoice.getSumainv().subtract(invoice2.getSumainv());
			invoiceRepository.save(invoice);
			
			Rezult rezult = rezultRepository.findOneByUserid(invoice.getUserid());
			rezult.setBank(rezult.getProvider().add(bigDecimal));
			rezult.setProvider(rezult.getProvider().add(bigDecimal));
			rezultRepository.save(rezult);
			
			Iterable<Commodity> commodityIter = commodityRepository.findAllByUserid (invoice.getUserid());
			boolean flag=false;
			for (Commodity commodity : commodityIter) {
				if (!flag && commodity.getInvoice()!=null &&commodity.getInvoice().getId()==invoice.getId()){
					flag=true;
				}
				if (flag){commodity.setBalance(commodity.getBalance().add(bigDecimal));}
			}flag=false;
			commodityRepository.save(commodityIter);
			Iterable<Provider> providerIter = providerRepository.findAllByUserid (invoice.getUserid());
			
			for (Provider provider : providerIter) {
				if (!flag && provider.getInvoice()!=null &&provider.getInvoice().getId()==invoice.getId()){
					flag=true;
				}
				if (flag){provider.setBalance(provider.getBalance().add(bigDecimal));}
			}flag=false;
			providerRepository.save(providerIter);			}
	}
}
