package ua.service;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ua.dao.IncomeOrder;
import ua.dao.Till;
import ua.repository.TillRepository;

@Service
public class TillService {
	@Autowired
	TillRepository tillRepository;
		
	public void save(Till t){
		tillRepository.save(t);
	}

	public void delete(int id) {
		tillRepository.delete(id);
	}

	public Iterable<Till> findAllByUserid(Principal principal) {
	/*List<IncomeOrder> list = tillRepository.findNew2(Integer.parseInt(principal.getName()));
	for (IncomeOrder incomeOrder : list) {
		System.out.println(incomeOrder.getId()+"_!!!_"+incomeOrder.getDates());
	}*/
	
	return tillRepository.findAllByUserid(Integer.parseInt(principal.getName()));
	}
	
}
