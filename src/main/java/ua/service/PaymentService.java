package ua.service;

import java.math.BigDecimal;
import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ua.dao.Bank;
import ua.dao.Payment;
import ua.dao.Provider;
import ua.dao.Rezult;
import ua.repository.BankRepository;
import ua.repository.FirmRepository;
import ua.repository.PaymentRepository;
import ua.repository.ProviderRepository;
import ua.repository.RezultRepository;

@Service
public class PaymentService {
	@Autowired
	PaymentRepository paymentRepository;
	@Autowired
	ProviderRepository providerRepository;
	@Autowired
	BankRepository bankRepository;
	@Autowired
	RezultRepository rezultRepository;
	@Autowired
	FirmRepository firmRepository;

	
	public void save (Payment payment){
		paymentRepository.save(payment);
		Rezult rezult = rezultRepository.findOneByUserid(payment.getUserid());
		rezult.setBank(rezult.getBank().subtract(payment.getSumapay()));
		rezult.setProvider(rezult.getProvider().add(payment.getSumapay()));
		rezultRepository.save(rezult);
		Bank bank = new Bank();
		bank.setUserid(payment.getUserid());
		bank.setPayment(payment);
		bank.setSpendOrder(null);
		bank.setBalance(rezult.getBank());
		bankRepository.save(bank);
		Provider provider = new Provider();
		provider.setUserid(payment.getUserid());
		provider.setPayment(payment);
		provider.setInvoice(null);
		provider.setBalance(rezult.getProvider());
		providerRepository.save(provider);
			}
	
	public void delete(int id) {
		paymentRepository.delete(id);
	}

	public Iterable<Payment> findAllByUserid(Principal principal) {
		return paymentRepository.findAllByUserid(Integer.parseInt(principal.getName()));
	}
	public Payment findById(Integer id) {
		return paymentRepository.findOne(id);
	}

	public void update(Payment payment) {
		Payment payment2 = paymentRepository.findOne(payment.getId());
		if(payment2.getSumapay()==payment.getSumapay()){
			paymentRepository.save(payment);}else{
			BigDecimal bigDecimal = payment.getSumapay().subtract(payment2.getSumapay());
			paymentRepository.save(payment);
			
			Rezult rezult = rezultRepository.findOneByUserid(payment.getUserid());
			rezult.setBank(rezult.getProvider().add(bigDecimal));
			rezult.setProvider(rezult.getProvider().add(bigDecimal));
			rezultRepository.save(rezult);
			
			Iterable<Provider> providerIter = providerRepository.findAllByUserid (payment.getUserid());
			boolean flag=false;
			for (Provider provider : providerIter) {
				if (!flag && provider.getPayment()!=null && provider.getPayment().getId()== payment.getId()){
					flag=true;
				}
				if (flag){provider.setBalance(provider.getBalance().add(bigDecimal));}
			}flag=false;
			providerRepository.save(providerIter);
			Iterable<Bank> bankIter = bankRepository.findAllByUserid(payment.getUserid());
			for (Bank bank : bankIter) {
				if(!flag && bank.getPayment()!=null && bank.getPayment().getId()==payment.getId()){
					flag=true;
				}
				if(flag){bank.setBalance(bank.getBalance().add(bigDecimal));}
			}
			bankRepository.save(bankIter);

			}	
	}
}
