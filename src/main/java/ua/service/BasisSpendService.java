package ua.service;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ua.dao.BasisSpend;
import ua.repository.BasisSpendRepository;

@Service
public class BasisSpendService {
	@Autowired
	BasisSpendRepository basisSpendRepository;
	@Autowired
	ChangeUtf8 changeUtf8;

	public void save(String basis) {
		BasisSpend basisSpend = new BasisSpend();
		basisSpend.setBasis(basis);
		basisSpendRepository.save(basisSpend);
	}

	public void save(BasisSpend basisSpend) {
		basisSpend.setBasis(changeUtf8.rewrite(basisSpend.getBasis(), "і"));
		basisSpendRepository.save(basisSpend);
	}

	public void delete(int id) {
		basisSpendRepository.delete(id);
	}

	public Iterable<BasisSpend> findAll() {
		return basisSpendRepository.findAll();
	}

	public Iterable<BasisSpend> findAllByUserid(Principal principal) {
		return basisSpendRepository.findAllByUserid(Integer.parseInt(principal.getName()));
	}

	public BasisSpend findById(Integer id) {
		return basisSpendRepository.findOne(id);
	}
}
