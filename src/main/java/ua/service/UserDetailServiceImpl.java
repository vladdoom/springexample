package ua.service;

import java.text.SimpleDateFormat;
//import java.net.PasswordAuthentication;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ua.dao.Users;
import ua.repository.UsersRepository;
import ua.utility.FormatDate;

@Service("userDetailsService")
public class UserDetailServiceImpl implements UserDetailsService {
	@Autowired
	private UsersRepository usersRepository;
	public FormatDate formatDate;

	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		Users user = usersRepository.findByUsername(login);
		if (user == null){
			return null;
			}
		Collection<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(user.getROLE()));
		String userid=String.valueOf(user.getId());
		System.out.println("User__"+userid);
		//user.setLastActivity(formatDate.getDate());
		user.setLastActivity(getDate());
		usersRepository.save(user);

		return new User(userid ,user.getPassword(),authorities);
	}
	public String getDate(){
		 Date date = new Date();
		  SimpleDateFormat formater = new SimpleDateFormat("dd.MM.yyyy");
		 return formater.format(date);
	}
}
