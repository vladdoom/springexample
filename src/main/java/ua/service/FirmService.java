package ua.service;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ua.dao.Firm;
import ua.repository.FirmRepository;

@Service
public class FirmService {
	@Autowired
	FirmRepository firmRepository;
	@Autowired
	ChangeUtf8 changeUtf8;

	public void save(Firm firm){
		firm.setName(changeUtf8.rewrite( firm.getName(), "і"));
		firmRepository.save(firm);
	}
	public void delete (int id){
		firmRepository.delete(id);
	}
	public Iterable<Firm> findAllByUserid(Principal principal){
		return firmRepository.findAllByUserid(Integer.parseInt(principal.getName()));
	}
	public Firm findById(Integer id) {
		return firmRepository.findOne(id);
	}
	public Iterable<String> findNameByUserId(Principal principal) {
		return firmRepository.findNameByUserid(Integer.parseInt(principal.getName()));
	}
}
