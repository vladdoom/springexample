package ua.service;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ua.dao.Bank;
import ua.repository.BankRepository;

@Service
public class BankService {
	@Autowired
	BankRepository bankRepository;

	
	public void save(Bank bank){
		bankRepository.save(bank);
	}
	public void delete(int id) {
		bankRepository.delete(id);
	}

	public Iterable<Bank> findAllByUserid(Principal principal) {
		return bankRepository.findAllByUserid(Integer.parseInt(principal.getName()));
	}
}
