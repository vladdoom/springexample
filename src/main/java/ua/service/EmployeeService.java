package ua.service;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ua.dao.Employee;
import ua.repository.EmployeeRepository;

@Service
public class EmployeeService {
	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	ChangeUtf8 changeUtf8;

	public void save(Principal principal, String name) {
		Employee e = new Employee();
		e.setUserid(Integer.parseInt(principal.getName()));
		e.setName(name);
		employeeRepository.save(e);
	}

	public void save(Employee e) {
		e.setName(changeUtf8.rewrite( e.getName(), "і"));
		employeeRepository.save(e);
	}

	public void delete(int id) {
		employeeRepository.delete(id);
	}

	public Iterable<Employee> findAllByUserid(Principal principal) {
		return employeeRepository.findAllByUserid(Integer.parseInt(principal.getName()));
	}

	public Employee findById(Integer id) {
		return employeeRepository.findOne(id);
	}
}
