package ua.repository;

import org.springframework.data.repository.CrudRepository;

import ua.dao.Payment;

public interface PaymentRepository extends CrudRepository<Payment, Integer>{
	public Iterable<Payment> findAllByUserid(int id);
}
