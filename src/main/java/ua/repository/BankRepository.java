package ua.repository;

import org.springframework.data.repository.CrudRepository;

import ua.dao.Bank;

public interface BankRepository extends CrudRepository<Bank, Integer>{
	public Iterable<Bank> findAllByUserid(int id);
}
