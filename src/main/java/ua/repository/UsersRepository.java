package ua.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import ua.dao.Users;

public interface UsersRepository extends CrudRepository<Users, Integer>{
	@Query("select u from Users u where u.username = :login")
	public Users findByUsername(@Param(value = "login") String login);
}
