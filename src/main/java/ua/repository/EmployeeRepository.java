package ua.repository;

import org.springframework.data.repository.CrudRepository;

import ua.dao.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Integer>{
	public Iterable<Employee> findAllByUserid(int id);
}
