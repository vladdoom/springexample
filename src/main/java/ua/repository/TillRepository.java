package ua.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import ua.dao.IncomeOrder;
import ua.dao.Till;

public interface TillRepository extends CrudRepository<Till, Integer> {
/*	@Query("select t.balance, i.dates, i.number, i.basis, i.suma, s.dates, s.number, s.basis, s.suma "
			+ " from Till t  "
			+ "full join IncomeOrder i on  i = t.incomeOrder"
			+ "full join SpendOrder  s  on s = t.spendOrder"
			+ "where t.userid = :id ")
	public Iterable<Till> findNew(@Param ("id")int id);
*/	/*@Query("select t, i,s from Till t  "
			+ "left join t.incomeOrder i "
			+ "left join t.spendOrder  s "
			+ "where t.userid = :id ")
	public List<Till> findNew2(@Param ("id")int id);*/
	
@Query("select  i from Till t  "
		+ "left join  t.incomeOrder i "
		+ "where t.userid = :id ")
public List<IncomeOrder> findNew2(@Param ("id")int id);
	
	
	public Iterable<Till> findAllByUserid(int id);
}
