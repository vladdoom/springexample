package ua.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import ua.dao.IncomeOrder;

public interface IncomeOrderRepository extends CrudRepository<IncomeOrder, Integer>{
	public Iterable<IncomeOrder> findAllByUserid(int id);
	//@Query("select i from IncomeOrder i  left join BasisIncome on where i.Userid = :id")
	//public Iterable<IncomeOrder> findAllByUseridSQL(@Param(value = "id") int id);
	
}
