package ua.repository;

import org.springframework.data.repository.CrudRepository;


import ua.dao.BasisIncome;

public interface BasisIncomeRepository extends CrudRepository<BasisIncome, Integer>{
	public Iterable<BasisIncome> findAllByUserid(int id);
}
