package ua.repository;

import org.springframework.data.repository.CrudRepository;

import ua.dao.Rezult;

public interface RezultRepository extends CrudRepository<Rezult, Integer>{
	public Rezult findOneByUserid(int id);

}
