package ua.repository;

import org.springframework.data.repository.CrudRepository;

import ua.dao.Income;

public interface IncomeRepository  extends CrudRepository<Income, Integer>{
	public Iterable<Income> findAllByUserid(int id);
}
