package ua.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import ua.dao.Firm;

public interface FirmRepository extends CrudRepository<Firm, Integer>{
	public Iterable<Firm> findAllByUserid(int id);
	
	@Query("select p.name  from Firm p where p.userid=:userid")
	public Iterable<String> findNameByUserid(@Param(value = "userid") int userid );
}
