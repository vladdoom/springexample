package ua.repository;

import org.springframework.data.repository.CrudRepository;

import ua.dao.Commodity;

public interface CommodityRepository extends CrudRepository<Commodity, Integer> {

	Iterable<Commodity> findAllByUserid(int id);
}
