package ua.repository;

import org.springframework.data.repository.CrudRepository;

import ua.dao.SpendOrder;

public interface SpendOrderRepository extends CrudRepository<SpendOrder, Integer>{
	public Iterable<SpendOrder> findAllByUserid(int id);
}
