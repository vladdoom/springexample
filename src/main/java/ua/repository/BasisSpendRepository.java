package ua.repository;

import org.springframework.data.repository.CrudRepository;


import ua.dao.BasisSpend;

public interface BasisSpendRepository extends CrudRepository<BasisSpend, Integer>{
public Iterable<BasisSpend> findAllByUserid(int id);
}
