package ua.repository;


import org.springframework.data.repository.CrudRepository;

import ua.dao.Provider;

public interface ProviderRepository extends CrudRepository<Provider, Integer>{
	public Iterable<Provider> findAllByUserid(int id);
}
