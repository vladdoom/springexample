package ua.repository;

import org.springframework.data.repository.CrudRepository;

import ua.dao.Invoice;

public interface InvoiceRepository extends CrudRepository<Invoice, Integer>{
	public Iterable<Invoice> findAllByUserid(int id);
}
