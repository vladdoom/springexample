<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
		<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>NanoAccount</title>
<spring:url value="/resources/css/main.css" var="mainCss" />
<spring:url value="/resources/images/logo.jpg" var="logo" />
<link href="${mainCss}" rel="stylesheet" />
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<%@include file="TopLine.jsp"%>
		</div>
		<div id="content">
			<div align="center" class="content">
				<table border="1">
					<tr>
						<th>#</th>
						<th>id</th>
						<th>User name</th>
						<th>email</th>
						<th>Role</th>
						<th>last Activity</th>
						<th>language</th>
					</tr>

					<c:forEach var="handeList" items="${handelist}" varStatus="status">
						<tr>
							<td>${status.index + 1}</td>
							<td>${handeList.id}</td>
							<td>${handeList.username}</td>
							<td>${handeList.emaile}</td>
							<td>${handeList.ROLE}</td>
							<td>${handeList.lastActivity}</td>
							<td>${handeList.language}</td>
							<td><a href="delete?id=${handeList.id}&document=${document}">Delete</a></td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>
		<div id="footer">
			<ul>
				<li><spring:message code="label.aegis" /></li>
				<li><spring:message code="label.support" />vladdoom@gmail.com</li>
			</ul>
		</div>
	</div>
</body>
</html>