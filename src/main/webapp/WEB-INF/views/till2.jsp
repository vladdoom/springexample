<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("UTF-8");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
		<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>NanoAccount</title>
<spring:url value="/resources/css/main.css" var="mainCss" />
<spring:url value="/resources/images/logo.jpg" var="logo" />
<link href="${mainCss}" rel="stylesheet" />

</head>
<body>
	<div id="wrapper">
		<div id="header">
			<%@include file="TopLine.jsp"%>
		</div>
		<div id="content">
			<div align="center" class="content">
				<table border="1">
					<tr>
						<td>#</td>
						<td><spring:message code="label.date" /></td>
						<td><spring:message code="label.number" /></td>
						<td><spring:message code="label.basis" /></td>
						<td><spring:message code="label.sumadt" /></td>
						<td><spring:message code="label.sumakt" /></td>
						<td><spring:message code="label.balance" /></td>
					</tr>
					<security:authorize access="isAuthenticated()">
						<c:forEach var="handeList" items="${handelist}" varStatus="status">
							<c:if test="${handeList.spendOrder ne null}">
								<tr>
									<td>${status.index + 1}</td>
									<td>${handeList.spendOrder.dates}</td>
									<td>${handeList.spendOrder.number}</td>
									<%-- <td>${handeList.spendOrder.basis.basis}</td> --%>
									<td>0.00</td>
									<td>${handeList.spendOrder.suma}</td>
									<td>${handeList.balance}</td>
								</tr>
							</c:if>
							<c:if test="${handeList.incomeOrder ne null}">
								<tr>
									<td>${status.index + 1}</td>
									<td>${handeList.incomeOrder.dates}</td>
									<td>${handeList.incomeOrder.number}</td>
									<%-- <td>${handeList.incomeOrder.basis.basis}</td> --%>
									<td>${handeList.incomeOrder.suma}</td>
									<td>0.00</td>
									<td>${handeList.balance}</td>
								</tr>
							</c:if>
						</c:forEach>
					</security:authorize>
				</table>
			</div>
		</div>
		<div id="footer">
			<ul>
				<li><spring:message code="label.aegis" /></li>
				<li><spring:message code="label.support" />vladdoom@gmail.com</li>
			</ul>
		</div>
	</div>
</body>
</html>