<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
		<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>NanoAccount</title>
<spring:url value="/resources/css/main.css" var="mainCss" />
<spring:url value="/resources/images/logo.jpg" var="logo" />
<link href="${mainCss}" rel="stylesheet" />
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<%@include file="TopLine.jsp"%>
		</div>
		<div id="content">
			<div align="center" class="content">
				<table border="1">
					<tr>
						<th>#</th>
						<th><spring:message code="label.number" /></th>
						<th><spring:message code="label.date" /></th>
						<th><spring:message code="label.firm" /></th>
						<th><spring:message code="label.suma" /></th>
						<th><spring:message code="label.notes" /></th>
					</tr>
					<security:authorize access="isAuthenticated()">
						<c:forEach var="handeList" items="${handelist}" varStatus="status">
							<tr>
								<td>${status.index + 1}</td>
								<td>${handeList.number}</td>
								<td>${handeList.date}</td>
								<td>${handeList.firmName.name}</td>
								<td>${handeList.sumapay}</td>
								<td>${handeList.notes}</td>
								<td><a
									href="editpayment?id=${handeList.id}&document=${document}">Edit</a>
								</td>
							</tr>
						</c:forEach>
					</security:authorize>
				</table>
			</div>
			<security:authorize access="isAuthenticated()">
				<div align="center" class="content">
					<form:form method="post" modelAttribute="payment"
						commandName="payment" acceptCharset="UTF-8">
						<table>
							<tr>
								<td><spring:message code="label.write" />&nbsp; <spring:message
										code="label.date" /></td>
								<td><form:input path="date" required="required"  /></td>
								<td><form:errors path="date" /></td>
							</tr>

							<tr>
								<td><spring:message code="label.write" />&nbsp; <spring:message
										code="label.number" /></td>
								<td><form:input path="number" required="required" /></td>
								<td><form:errors path="number" /></td>
							</tr>
							<tr>
								<td><spring:message code="label.choice" />&nbsp; <spring:message
										code="label.firm" /></td>
								<td><form:select path="firmName">
										<form:options items="${listfirm}" itemLabel="name"
											itemValue="id"></form:options>
									</form:select></td>
								<td></td>
							</tr>
							<tr>
								<td><spring:message code="label.write" />&nbsp; <spring:message
										code="label.suma" /></td>
								<td><form:input path="sumatransient" required="required" /></td>
								<td><form:errors path="sumatransient" cssClass="errors" /></td>
							</tr>
							<tr>
								<td><spring:message code="label.write" />&nbsp; <spring:message
										code="label.notes" /></td>
								<td><form:input path="notes" type="text"
										required="required" /></td>
								<td><form:errors path="notes" /></td>
							</tr>
							<tr>
								<td colspan="2" align="center"><input type="submit"
									value="Save"></td>
							</tr>
						</table>
					</form:form>
				</div>
			</security:authorize>
		</div>
		<div class="bottomdiv"></div>
		<div id="footer">
			<ul>
				<li><spring:message code="label.aegis" /></li>
				<li><spring:message code="label.support" />vladdoom@gmail.com</li>
			</ul>
		</div>
	</div>
</body>
</html>