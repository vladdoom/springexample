<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("UTF-8");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>
<meta http-equiv="Content-Type"
	content="text/html; charset=UTF-8">
<spring:url value="/resources/images/logo.jpg" var="logo" />
</head>
<body>
<nav class="menu">
<ul>
	<li><a href="income"><spring:message code="label.income" /></a></li>
	<li><a href="incomeorder"><spring:message
				code="label.incomeorder" /></a></li>
	<li><a href="till"><spring:message code="label.till" /></a></li>
	<li><a href="spendorder"><spring:message
				code="label.spendorder" /></a></li>
	<li><a href="bank"><spring:message code="label.bank" /></a></li>
	<li><a href="payment"><spring:message code="label.payment" /></a></li>
	<li><a href="provider"><spring:message code="label.provider" /></a></li>
	<li><a href="invoice"><spring:message code="label.invoice" /></a></li>
	<li><a href="commodity"><spring:message code="label.commodity" /></a></li>
</ul>

</nav>
<nav class="submenu">
<ul>
	<li class="lang"><spring:message code="label.lang" />
		<ul>
			<li><a href="?lang=ua">ua</a></li>
			<li><a href="?lang=en">en</a></li>
			<li><a href="?lang=ru">ru</a></li>
		</ul>
	<li><a href="homepage"><spring:message code="label.homepage" /></a></li>
	<li><a href="basisSpend"><spring:message
				code="label.basisSpend" /></a></li>
	<li><a href="basisIncome"><spring:message
				code="label.basisIncome" /></a></li>
	<li><a href="firm"><spring:message code="label.firm" /> </a></li>
	<li><a href="employee"><spring:message code="label.employee" /></a></li>
	<li><security:authorize access="hasRole('ROLE_ADMIN')">
			<a href="reglist">List Registration Users</a>
		</security:authorize></li>
	<li><security:authorize access="isAuthenticated()">
			<c:url value="/logout" var="logout" />
			<form:form method="post" action="${logout}">
				<input type="submit" value="LOGOUT" class="logout">
			</form:form>
		</security:authorize></li>
</ul>
</nav>

</body>
</html>