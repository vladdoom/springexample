<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("UTF-8");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
		<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
	
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>NanoAccount</title>
<spring:url value="/resources/css/main.css" var="mainCss" />
<spring:url value="/resources/images/logo.jpg" var="logo" />
<link href="${mainCss}" rel="stylesheet" />

</head>
<body>
	<div id="wrapper">
		<div id="header">
			<%@include file="TopLine.jsp"%>
		</div>
		<div id="content">
			<div align="center" class="content">
				<table border="1">
					<tr>
						<td>#</td>
						<td><spring:message code="label.firm" /></td>
						<td><spring:message code="label.number" /></td>
						<td><spring:message code="label.date" /></td>
						<td><spring:message code="label.suma" /></td>
						<td><spring:message code="label.pdv" /></td>
					</tr>
					<security:authorize access="isAuthenticated()">
						<c:forEach var="handeList" items="${handelist}" varStatus="status">
							<tr>
								<td>${status.index + 1}</td>
								<td>${handeList.firmName.name}</td>
								<td>${handeList.number}</td>
								<td>${handeList.date}</td>
								<td>${handeList.sumainv}</td>
								<td>${handeList.pdv}</td>
								<td><a href="editinvoice?id=${handeList.id}">Edit</a></td>
							</tr>
						</c:forEach>
					</security:authorize>
				</table>
			</div>
			<security:authorize access="isAuthenticated()">
				<div align="center" class="content">
					<form:form method="post" modelAttribute="invoice">
						<table>
							<tr>
								<td><spring:message code="label.choice" />&nbsp; <spring:message
										code="label.firm" /></td>
								<td><form:select path="firmName">
										<form:options items="${listfirm}" itemLabel="name"
											itemValue="id"></form:options>
									</form:select></td>
							</tr>
							<tr>
								<td><spring:message code="label.write" />&nbsp; <spring:message
										code="label.number" /></td>
								<td><form:input path="number" required="required" /></td>
							</tr>
							<tr>
								<td><spring:message code="label.write" />&nbsp; <spring:message
										code="label.date" /></td>
								<td><form:input path="date" required="required" />
							</tr>
							<tr>
								<td><spring:message code="label.write" />&nbsp; <spring:message
										code="label.suma" /></td>
								<td><form:input path="sumatransient" placeholder="0.00"
										required="required" /></td>
								<td><form:errors path="sumatransient" /></td>
							</tr>
							<tr>
								<td><spring:message code="label.write" />&nbsp; <spring:message
										code="label.pdv" /></td>
								<td><form:input path="pdvtransient" placeholder="0.00"
										required="required" /></td>
								<td><form:errors path="pdvtransient" /></td>
							</tr>
							<tr>
								<td colspan="2" align="center"><input type="submit"
									value="Enter to save"></td>
							</tr>
						</table>
					</form:form>
				</div>
			</security:authorize>
		</div>
		<div id="footer">
			<ul>
				<li><spring:message code="label.aegis" /></li>
				<li><spring:message code="label.support" />vladdoom@gmail.com</li>
			</ul>
		</div>
	</div>
</body>
</html>