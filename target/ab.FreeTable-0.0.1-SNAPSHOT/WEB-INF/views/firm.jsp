<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("UTF-8");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
		<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>NanoAccount</title>
<spring:url value="/resources/css/main.css" var="mainCss" />
<spring:url value="/resources/images/logo.jpg" var="logo" />
<link href="${mainCss}" rel="stylesheet" />

</head>
<body>
	<div id="wrapper">
		<div id="header">
			<%@include file="TopLine.jsp"%>
		</div>
		<div id="content">
			<div align="center" class="content">
				<table border="1">
					<tr>
						<th>#</th>
						<th><spring:message code="label.firm" /></th>
						<th><spring:message code="label.bill" /></th>
						<th><spring:message code="label.EDRPOU" /></th>
						<th><spring:message code="label.MFO" /></th>
					</tr>
					<security:authorize access="isAuthenticated()">
						<c:forEach var="handeList" items="${handelist}" varStatus="status">
							<tr>
								<td>${status.index + 1}</td>
								<td>${handeList.name}</td>
								<td>${handeList.bill}</td>
								<td>${handeList.edrpou}</td>
								<td>${handeList.mfo}</td>
								<td><a
									href="editFirm?id=${handeList.id}&document=${document}">Edit</a>
									<%-- &nbsp;&nbsp;&nbsp;&nbsp; 
						<a href="delete?id=${handeList.id}&document=${document}">Delete</a> --%></td>
							</tr>
						</c:forEach>
					</security:authorize>
				</table>
			</div>
			<security:authorize access="isAuthenticated()">
				<div align="center" class="content">
					<form:form method="post" modelAttribute="firm"
						acceptCharset="UTF-8">
						<table>
							<tr>
								<td><spring:message code="label.write" />&nbsp; <spring:message
										code="label.firm" /></td>
								<td><form:input path="name" required="required" /></td>
							</tr>
							<tr>
								<td><spring:message code="label.write" />&nbsp; <spring:message
										code="label.bill" /></td>
								<td><form:input path="bill" /></td>
							</tr>
							<tr>
								<td><spring:message code="label.write" />&nbsp; <spring:message
										code="label.EDRPOU" /></td>
								<td><form:input path="edrpou" /></td>
							</tr>
							<tr>
								<td><spring:message code="label.write" />&nbsp; <spring:message
										code="label.MFO" /></td>
								<td><form:input path="mfo" /></td>
							</tr>
							<tr>
								<td colspan="2" align="center"><input type="submit"
									value="Save"></td>
							</tr>
						</table>
					</form:form>
				</div>
			</security:authorize>
		</div>
		<div id="footer">
			<ul>
				<li><spring:message code="label.aegis" /></li>
				<li><spring:message code="label.support" />vladdoom@gmail.com</li>
			</ul>
		</div>
	</div>
</body>
</html>