<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
		<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>NanoAccount</title>
<spring:url value="/resources/css/main.css" var="mainCss" />
<spring:url value="/resources/images/logo.jpg" var="logo" />
<link href="${mainCss}" rel="stylesheet" />
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<%@include file="TopLine.jsp"%>
		</div >
		<div id="content">
			<div align="center" class="content">
				<table border="1">
					<tr>
						<td>#</td>
						<td><spring:message code="label.basisSpend" /></td>
					</tr>
					<security:authorize access="isAuthenticated()">
						<c:forEach var="handeList" items="${handelist}" varStatus="status">
							<tr>
								<td>${status.index + 1}</td>
								<td>${handeList.basis}</td>
								<td><a
									href="editbasisSpend?id=${handeList.id}&document=${document}">Edit</a>
									<%-- &nbsp;&nbsp;&nbsp;&nbsp; 
						<a href="delete?id=${handeList.id}&document=${document}">Delete</a> --%></td>
							</tr>
						</c:forEach>
					</security:authorize>
				</table>
			</div>
			<security:authorize access="isAuthenticated()">
				<div align="center" class="content bottomdiv">
					<form:form method="post" modelAttribute="basisSpend" acceptCharset="UTF-8">
	<%
	request.setCharacterEncoding("UTF-8");
	String basis = request.getParameter("basis");
%>					
						<table>
							<tr>
								<td><spring:message code="label.basisSpendWrite" /></td>
								<td char="UTF-8" charoff="UTF-8" >
								<form:input path="basis" 
										required="required"  /></td>
							</tr>
							<tr>
								<td colspan="2" align="center"><input type="submit"
									value="Save"></td>
							</tr>
						</table>
					</form:form>
				</div>
			</security:authorize>
		</div>
		<div class="bottomdiv"></div>
		<div id="footer">
			<ul>
				<li>
				  © <spring:message code="label.aegis" /></li>
				<li><spring:message code="label.support" />vladdoom@gmail.com</li>
			</ul>
		</div>
	</div>
</body>
</html>