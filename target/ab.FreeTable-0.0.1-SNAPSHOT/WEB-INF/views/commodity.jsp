<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("UTF-8");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
		<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>NanoAccount</title>
<spring:url value="/resources/css/main.css" var="mainCss" />
<spring:url value="/resources/images/logo.jpg" var="logo" />
<link href="${mainCss}" rel="stylesheet" />
</head>

<body>
	<div id="wrapper">
		<div id="header">
			<%@include file="TopLine.jsp"%>
		</div>
		<div id="content">
			<div align="center" class="content">
				<table border="1">
					<tr>
						<th>#</th>
						<th><spring:message code="label.firm" /></th>
						<th><spring:message code="label.number" /></th>
						<th><spring:message code="label.date" /></th>
						<th><spring:message code="label.sumadt" /></th>
						<th><spring:message code="label.sumakt" /></th>
						<th><spring:message code="label.balance" /></th>
					</tr>
					<security:authorize access="isAuthenticated()">
						<c:forEach var="handeList" items="${handelist}" varStatus="status">
							<tr>
								<td>${status.index + 1}</td>
								<td>${handeList.invoice.firmName.name}</td>
								<td>${handeList.invoice.number}</td>
								<td>${handeList.invoice.date}</td>
								<td>${handeList.invoice.sumainv}</td>
								<td>0.00</td>
								<td>${handeList.balance}</td>
							</tr>
						</c:forEach>
					</security:authorize>
				</table>
			</div>
		</div>
		<div id="footer">
			<ul>
				<li><spring:message code="label.aegis" /></li>
				<li><spring:message code="label.support" />vladdoom@gmail.com</li>
			</ul>
		</div>
	</div>
</body>
</html>