<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
		<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>NanoAccount</title>
<%-- <spring:url value="/resources/css/main.css" var="mainCss" />
<spring:url value="/resources/images/logo.jpg" var="logo" />
<link href="${mainCss}" rel="stylesheet" />
 --%></head>
<body>
	<div id="wrapper">
		<div id="header">
			<%@include file="TopLine.jsp"%>
		</div>
		<div id="content">
		<c:if test="${param.authfail eq true }">
			<div align="center" class="content">
				You write failed login or password. Try again.
			</div>
			</c:if>
			<security:authorize access="!isAuthenticated()">
				<div align="center" class="content">
					<h1>Go to your cabinet</h1>
					<c:url value="/login" var="login" />
					<form:form method="post" action="${login}">
						<table>
							<tr>
								<td>Enter your name:</td>
								<td><input type="text" name="username" value="Pit"/></td>
								<td><form:errors path="username" /></td>
							</tr>
							<tr>
								<td>Enter your password:</td>
								<td><input type="password" name="password" value="123"/></td>
								<td><form:errors path="password" ></form:errors></td>
							</tr>
							<tr>
								<td colspan="2" align="center"><input type="submit"
									value="Enter to cabinet"></td>
							</tr>
						</table>
					</form:form>
				</div>
			</security:authorize>
			<security:authorize access="!isAuthenticated()">
				<div align="center" class="content">
					<h1>Form Registration</h1>
					<form:form method="post" action="registration"
						modelAttribute="newUser">
						<table>
							<tr>
								<td>Enter your name:</td>
								<td><form:input type="text" path="username" /></td>
								<td><form:errors path="username" /></td>
							</tr>
							<tr>
								<td>Enter your e-mail:</td>
								<td><form:input type="text" path="emaile"
										pattern="\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6}" /></td>
								<td><form:errors path="emaile" /></td>
							</tr>
							<tr>
								<td>Enter your password:</td>
								<td><form:input type="password" path="password" /></td>
								<td><form:errors path="password" /></td>
							</tr>
							<tr>
							<tr>
								<td colspan="2" align="center"><input type="submit"
									value="Registration"></td>
							</tr>
						</table>

					</form:form>
				</div>
			</security:authorize>
		</div>
		<div id="footer">
			<ul>
				<%-- <li><img src="${logo}" style="height:50%;"></li> --%>
				<li><spring:message code="label.aegis" /></li>
				<li><spring:message code="label.support" />vladdoom@gmail.com</li>
			</ul>
		</div>
	</div>
</body>
</html>